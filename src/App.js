import "./App.css";
import MainRouter from "./Components/Router/MainRoute";

function App() {
  return (
    <div>
      <MainRouter />
    </div>
  );
}

export default App;
