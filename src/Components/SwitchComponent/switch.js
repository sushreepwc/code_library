import * as React from "react";
import Typography from "@mui/material/Typography";
import CodePen from "../Icons/codepen.png";
import Award from "../Icons/award.png";
import { Switch } from "pwc-react-library";
import { Grid } from "@mui/material";

const switchImport = `import { Switch } from "pwc-react-library";`;
const defaultChecked = `<Switch size="large" switch_type="FullToggle" defaultChecked />`;
const nonDefaultChecked = `<Switch size="large" switch_type="FullToggle" />`;
const disabledDefaultChecked = `<Switch backgroundColor="danger" defaultChecked disabled size="large" switch_type="FullToggle" />`;
const nonDisabledDefaultChecked = `<Switch backgroundColor="danger" disabled label="" size="large" switch_type="FullToggle" />`;
const checkedLabel = `<Switch backgroundColor="default" defaultChecked label="Label" size="large" switch_type="FullToggle" />`;
const disabledLabel = `<Switch backgroundColor="default" defaultChecked label="Disabled" size="large" switch_type="FullToggle" disabled />`;
const smallSwitch = `<Switch size="small" switch_type="FullToggle" defaultChecked />`;
const largeSwitch = `<Switch size="large" switch_type="FullToggle" defaultChecked />`;
const FullToggle = `<Switch size="large" switch_type="FullToggle" label="FullToggle" />`;
const HalfToggle = `<Switch size="large" switch_type="HalfToggle" label="HalfToggle" />`;
const SmallToggle = `<Switch size="small" switch_type="SmallToggle" label="SmallToggle" />`;
const FixToggle = `<Switch size="large" switch_type="FixToggle" label="FixToggle" />`;
const defaultColor = `<Switch backgroundColor="default" defaultChecked label="Default" size="large" switch_type="FullToggle" />`;
const primaryColor = `<Switch backgroundColor="primary" defaultChecked label="Primary" size="large" switch_type="FullToggle" />`;
const secondaryColor = `<Switch backgroundColor="secondary" defaultChecked label="Secondary" size="large" switch_type="FullToggle" />`;
const dangerColor = `<Switch backgroundColor="danger" defaultChecked label="Danger" size="large" switch_type="FullToggle" />`;

export default function RatingComponent() {
  const [showBasicSwitches, setShowBasicSwitches] = React.useState(false);
  const [showLabelSwitches, setShowLabelSwitches] = React.useState(false);
  const [showSizeSwitches, setShowSizeSwitches] = React.useState(false);
  const [showTypeOfToggle, setShowTypeOfToggle] = React.useState(false);
  const [showSwitchColors, setShowSwitchColors] = React.useState(false);

  return (
    <div>
      <Typography
        variant="h5"
        component="h5"
        role="heading"
        aria-level={5}
        className="topHeading"
      >
        Switch
      </Typography>
      <Typography component="p" className="topHeadingSub">
        Switches toggle the state of a single setting on or off.
      </Typography>
      <Typography
        variant="h6"
        component="div"
        style={{
          marginTop: 30,
          display: "flex",
          position: "relative",
        }}
        role="heading"
        aria-level={6}
        className="titleHeading"
        gutterBottom
      >
        <img
          src={CodePen}
          alt="codePen"
          width="40px"
          height="40px"
          style={{ marginRight: "14px" }}
        />{" "}
        Import
      </Typography>
      <p
        style={{
          background: "#2D2D2D",
          height: "auto",
          width: "auto",
          borderRadius: 11,
          boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
        }}
      >
        <div
          style={{
            fontFamily: "Helvetica Neue LT Pro",
            fontStyle: "normal",
            fontWeight: 500,
            fontSize: 16,
            color: "#E1C16E",
            padding: "20px",
            whiteSpace: "break-spaces",
          }}
        >
          {switchImport}
        </div>
      </p>
      <Typography
        variant="h6"
        component="div"
        style={{ display: "flex", position: "relative" }}
        role="heading"
        aria-level={6}
        className="titleHeading"
      >
        <img
          src={Award}
          alt="Award"
          width="40px"
          height="40px"
          style={{ marginRight: "14px" }}
        />
        Basic switch
      </Typography>
      <br />
      <p
        style={{
          height: "auto",
          width: "auto",
          borderRadius: 6.5,
          border: "2px solid grey",
          alignItems: "center",
          display: "flex",
          justifyContent: "center",
          position: "relative",
        }}
      >
        <div
          style={{
            fontFamily: "Helvetica Neue LT Pro",
            fontStyle: "normal",
            fontWeight: 500,
            fontSize: 16,
            padding: "20px",
          }}
        >
          <Grid container spacing={2}>
            <Grid item>
              <Switch size="large" switch_type="FullToggle" defaultChecked />
            </Grid>
            <Grid item>
              <Switch size="large" switch_type="FullToggle" />
            </Grid>
            <Grid item>
              <Switch
                backgroundColor="danger"
                defaultChecked
                disabled
                size="large"
                switch_type="FullToggle"
              />
            </Grid>
            <Grid item>
              <Switch
                backgroundColor="danger"
                disabled
                size="large"
                switch_type="FullToggle"
              />
            </Grid>
          </Grid>
          <div className="show">
            <button
              onClick={() => setShowBasicSwitches(!showBasicSwitches)}
              className="btn"
            >
              {showBasicSwitches ? "hide code" : "show code"}
            </button>
          </div>
        </div>
      </p>
      {showBasicSwitches && (
        <div>
          <p
            style={{
              background: "#2D2D2D",
              height: "auto",
              width: "auto",
              borderRadius: 11,
              boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
            }}
          >
            <div
              style={{
                fontFamily: "Helvetica Neue LT Pro",
                fontStyle: "normal",
                fontWeight: 500,
                fontSize: 16,
                color: "#E1C16E",
                padding: "20px",
                whiteSpace: "break-spaces",
              }}
            >
              {defaultChecked}
              <br />
              {nonDefaultChecked}
              <br />
              {disabledDefaultChecked}
              <br />
              {nonDisabledDefaultChecked}
            </div>
          </p>
        </div>
      )}
      <br />
      <Typography
        variant="h6"
        component="div"
        style={{ display: "flex", position: "relative" }}
        role="heading"
        aria-level={6}
        className="titleHeading"
      >
        Label
      </Typography>
      <Typography component="p">
        You can provide a label to the Switch
      </Typography>
      <br />
      <p
        style={{
          height: "auto",
          width: "auto",
          borderRadius: 6.5,
          border: "2px solid grey",
          alignItems: "center",
          display: "flex",
          justifyContent: "center",
          position: "relative",
        }}
      >
        <div
          style={{
            fontFamily: "Helvetica Neue LT Pro",
            fontStyle: "normal",
            fontWeight: 500,
            fontSize: 16,
            padding: "20px",
          }}
        >
          <Switch
            backgroundColor="default"
            defaultChecked
            label="Label"
            size="large"
            switch_type="FullToggle"
          />
          <br />
          <Switch
            backgroundColor="default"
            defaultChecked
            disabled
            label="Disabled"
            size="large"
            switch_type="FullToggle"
          />
          <div className="show">
            <button
              onClick={() => setShowLabelSwitches(!showLabelSwitches)}
              className="btn"
            >
              {showLabelSwitches ? "hide code" : "show code"}
            </button>
          </div>
        </div>
      </p>
      {showLabelSwitches && (
        <div>
          <p
            style={{
              background: "#2D2D2D",
              height: "auto",
              width: "auto",
              borderRadius: 11,
              boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
            }}
          >
            <div
              style={{
                fontFamily: "Helvetica Neue LT Pro",
                fontStyle: "normal",
                fontWeight: 500,
                fontSize: 16,
                color: "#E1C16E",
                padding: "20px",
                whiteSpace: "break-spaces",
              }}
            >
              {checkedLabel}
              <br />
              {disabledLabel}
            </div>
          </p>
        </div>
      )}
      <br />
      <Typography
        variant="h6"
        component="div"
        style={{ display: "flex", position: "relative" }}
        role="heading"
        aria-level={6}
        className="titleHeading"
      >
        Sizes
      </Typography>
      <Typography component="p">
        Use the size prop to change the size of the switched.
      </Typography>
      <br />
      <p
        style={{
          height: "auto",
          width: "auto",
          borderRadius: 6.5,
          border: "2px solid grey",
          alignItems: "center",
          display: "flex",
          justifyContent: "center",
          position: "relative",
        }}
      >
        <div
          style={{
            fontFamily: "Helvetica Neue LT Pro",
            fontStyle: "normal",
            fontWeight: 500,
            fontSize: 16,
            padding: "20px",
          }}
        >
          <Switch
            size="small"
            switch_type="FullToggle"
            label="Small"
            defaultChecked
          />
          <br />
          <Switch
            size="large"
            switch_type="FullToggle"
            label="Large"
            defaultChecked
          />
          <div className="show">
            <button
              onClick={() => setShowSizeSwitches(!showSizeSwitches)}
              className="btn"
            >
              {showSizeSwitches ? "hide code" : "show code"}
            </button>
          </div>
        </div>
      </p>
      {showSizeSwitches && (
        <div>
          <p
            style={{
              background: "#2D2D2D",
              height: "auto",
              width: "auto",
              borderRadius: 11,
              boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
            }}
          >
            <div
              style={{
                fontFamily: "Helvetica Neue LT Pro",
                fontStyle: "normal",
                fontWeight: 500,
                fontSize: 16,
                color: "#E1C16E",
                padding: "20px",
                whiteSpace: "break-spaces",
              }}
            >
              {smallSwitch}
              <br />
              {largeSwitch}
            </div>
          </p>
        </div>
      )}
      <br />
      <Typography
        variant="h6"
        component="div"
        style={{ display: "flex", position: "relative" }}
        role="heading"
        aria-level={6}
        className="titleHeading"
      >
        Type of switches
      </Typography>
      <Typography component="p">
        Use the switch_type prop to change the type of switches.
      </Typography>
      <br />
      <p
        style={{
          height: "auto",
          width: "auto",
          borderRadius: 6.5,
          border: "2px solid grey",
          alignItems: "center",
          display: "flex",
          justifyContent: "center",
          position: "relative",
        }}
      >
        <div
          style={{
            fontFamily: "Helvetica Neue LT Pro",
            fontStyle: "normal",
            fontWeight: 500,
            fontSize: 16,
            padding: "20px",
          }}
        >
          <Switch size="large" switch_type="FullToggle" label="FullToggle" />
          <br />
          <Switch size="large" switch_type="HalfToggle" label="HalfToggle" />
          <br />
          <Switch size="small" switch_type="SmallToggle" label="SmallToggle" />
          <br />
          <Switch size="large" switch_type="FixToggle" label="FixToggle" />
          <div className="show">
            <button
              onClick={() => setShowTypeOfToggle(!showTypeOfToggle)}
              className="btn"
            >
              {showTypeOfToggle ? "hide code" : "show code"}
            </button>
          </div>
        </div>
      </p>
      {showTypeOfToggle && (
        <div>
          <p
            style={{
              background: "#2D2D2D",
              height: "auto",
              width: "auto",
              borderRadius: 11,
              boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
            }}
          >
            <div
              style={{
                fontFamily: "Helvetica Neue LT Pro",
                fontStyle: "normal",
                fontWeight: 500,
                fontSize: 16,
                color: "#E1C16E",
                padding: "20px",
                whiteSpace: "break-spaces",
              }}
            >
              {FullToggle}
              <br />
              {HalfToggle}
              <br />
              {SmallToggle}
              <br />
              {FixToggle}
            </div>
          </p>
        </div>
      )}
      <br />
      <Typography
        variant="h6"
        component="div"
        style={{ display: "flex", position: "relative" }}
        role="heading"
        aria-level={6}
        className="titleHeading"
      >
        Color
      </Typography>
      <Typography component="p">
        Use the color prop to change the color of the switches.
      </Typography>
      <br />
      <p
        style={{
          height: "auto",
          width: "auto",
          borderRadius: 6.5,
          border: "2px solid grey",
          alignItems: "center",
          display: "flex",
          justifyContent: "center",
          position: "relative",
        }}
      >
        <div
          style={{
            fontFamily: "Helvetica Neue LT Pro",
            fontStyle: "normal",
            fontWeight: 500,
            fontSize: 16,
            padding: "20px",
          }}
        >
          <Switch
            backgroundColor="default"
            defaultChecked
            label="Default"
            size="large"
            switch_type="FullToggle"
          />
          <br />
          <Switch
            backgroundColor="primary"
            defaultChecked
            label="Primary"
            size="large"
            switch_type="FullToggle"
          />
          <br />
          <Switch
            backgroundColor="secondary"
            defaultChecked
            label="Secondary"
            size="large"
            switch_type="FullToggle"
          />
          <br />
          <Switch
            backgroundColor="danger"
            defaultChecked
            label="Danger"
            size="large"
            switch_type="FullToggle"
          />
          <div className="show">
            <button
              onClick={() => setShowSwitchColors(!showSwitchColors)}
              className="btn"
            >
              {showSwitchColors ? "hide code" : "show code"}
            </button>
          </div>
        </div>
      </p>
      {showSwitchColors && (
        <div>
          <p
            style={{
              background: "#2D2D2D",
              height: "auto",
              width: "auto",
              borderRadius: 11,
              boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
            }}
          >
            <div
              style={{
                fontFamily: "Helvetica Neue LT Pro",
                fontStyle: "normal",
                fontWeight: 500,
                fontSize: 16,
                color: "#E1C16E",
                padding: "20px",
                whiteSpace: "break-spaces",
              }}
            >
              {defaultColor}
              <br />
              {primaryColor}
              <br />
              {secondaryColor}
              <br />
              {dangerColor}
            </div>
          </p>
        </div>
      )}
    </div>
  );
}
