import React from "react";
import { Typography, Card, CardContent } from "@mui/material/";
import Award from "../Icons/award.png";

export const DefaultViewComponent = ({ title, description, ...props }) => {
  return (
    <div>
      <Typography variant="h6" component="div" style={{ marginLeft: 10, display: "flex", position: "relative" }} role="heading" aria-level={6} className="titleHeading">
        <img src={Award} alt="Award" width="40px" height="40px" style={{ marginRight: "14px" }} />
        {title}
      </Typography>
      <Typography component="p" style={{ marginBottom: 30, marginLeft: 67 }} className="topHeadingSub">
        {description}
      </Typography>
      {props.children}
    </div>
  );
};
