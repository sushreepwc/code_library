import * as React from "react";
import Typography from "@mui/material/Typography";
import CodePen from "../Icons/codepen.png";
import Award from "../Icons/award.png";
import { CheckBox } from "pwc-react-library";
import { Grid } from "@mui/material";

const checkboxImport = `import { Checkbox } from "pwc-react-library";`;
const defaultChecked = `<CheckBox backgroundColor="primary" isChecked label="" size="small" />`;
const nonChecked = `<CheckBox backgroundColor="primary" label="" size="small" />`;
const nonCheckDisabled = `<CheckBox backgroundColor="primary" disabled label="" size="small" />`;
const checkedDisabled = `<CheckBox backgroundColor="primary" disabled isChecked label="" size="small" />`;
const checkedLabel = `<CheckBox backgroundColor="primary" isChecked label="Label" size="small" />`;
const disabledLabel = `<CheckBox backgroundColor="primary" disabled label="Disabled" size="small" />`;
const smallCheckbox = `<CheckBox backgroundColor="primary" isChecked label="Small" size="small" />`;
const mediumCheckbox = `<CheckBox backgroundColor="primary" isChecked label="Medium" size="medium" />`;
const largeCheckbox = `<CheckBox backgroundColor="primary" isChecked label="Large" size="large" />`;
const defaultColor = `<CheckBox backgroundColor="default" isChecked label="Default" size="small" />`;
const primaryColor = `<CheckBox backgroundColor="primary" isChecked label="Primary" size="small" />`;
const secondaryColor = `<CheckBox backgroundColor="secondary" isChecked label="Secondary" size="small" />`;
const dangerColor = `<CheckBox backgroundColor="danger" isChecked label="Danger" size="small" />`;

export default function RatingComponent() {
  const [showBasicSwitches, setShowBasicSwitches] = React.useState(false);
  const [showCheckboxLabel, setShowCheckboxLabel] = React.useState(false);
  const [showCheckboxSize, setShowCheckboxSize] = React.useState(false);
  const [showCheckboxColor, setShowCheckboxColor] = React.useState(false);

  return (
    <div>
      <Typography
        variant="h5"
        component="h5"
        role="heading"
        aria-level={5}
        className="topHeading"
      >
        Checkbox
      </Typography>
      <Typography component="p" className="topHeadingSub">
        Checkboxes allow the user to select one or more items from a set.
        Checkboxes can be used to turn an option on or off.
      </Typography>
      <Typography
        variant="h6"
        component="div"
        style={{
          marginTop: 30,
          display: "flex",
          position: "relative",
        }}
        role="heading"
        aria-level={6}
        className="titleHeading"
        gutterBottom
      >
        <img
          src={CodePen}
          alt="codePen"
          width="40px"
          height="40px"
          style={{ marginRight: "14px" }}
        />{" "}
        Import
      </Typography>
      <p
        style={{
          background: "#2D2D2D",
          height: "auto",
          width: "auto",
          borderRadius: 11,
          boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
        }}
      >
        <div
          style={{
            fontFamily: "Helvetica Neue LT Pro",
            fontStyle: "normal",
            fontWeight: 500,
            fontSize: 16,
            color: "#E1C16E",
            padding: "20px",
            whiteSpace: "break-spaces",
          }}
        >
          {checkboxImport}
        </div>
      </p>
      <Typography
        variant="h6"
        component="div"
        style={{ display: "flex", position: "relative" }}
        role="heading"
        aria-level={6}
        className="titleHeading"
      >
        <img
          src={Award}
          alt="Award"
          width="40px"
          height="40px"
          style={{ marginRight: "14px" }}
        />
        Basic checkboxes
      </Typography>
      <br />
      <p
        style={{
          height: "auto",
          width: "auto",
          borderRadius: 6.5,
          border: "2px solid grey",
          alignItems: "center",
          display: "flex",
          justifyContent: "center",
          position: "relative",
        }}
      >
        <div
          style={{
            fontFamily: "Helvetica Neue LT Pro",
            fontStyle: "normal",
            fontWeight: 500,
            fontSize: 16,
            padding: "20px",
          }}
        >
          <Grid container spacing={2}>
            <Grid item>
              <CheckBox
                backgroundColor="primary"
                isChecked
                label=""
                size="small"
              />
            </Grid>
            <Grid item>
              <CheckBox backgroundColor="primary" label="" size="small" />
            </Grid>
            <Grid item>
              <CheckBox
                backgroundColor="primary"
                disabled
                label=""
                size="small"
              />
            </Grid>
            <Grid item>
              <CheckBox
                backgroundColor="primary"
                disabled
                isChecked
                label=""
                size="small"
              />
            </Grid>
          </Grid>
          <div className="show">
            <button
              onClick={() => setShowBasicSwitches(!showBasicSwitches)}
              className="btn"
            >
              {showBasicSwitches ? "hide code" : "show code"}
            </button>
          </div>
        </div>
      </p>
      {showBasicSwitches && (
        <div>
          <p
            style={{
              background: "#2D2D2D",
              height: "auto",
              width: "auto",
              borderRadius: 11,
              boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
            }}
          >
            <div
              style={{
                fontFamily: "Helvetica Neue LT Pro",
                fontStyle: "normal",
                fontWeight: 500,
                fontSize: 16,
                color: "#E1C16E",
                padding: "20px",
                whiteSpace: "break-spaces",
              }}
            >
              {defaultChecked}
              <br />
              {nonChecked}
              <br />
              {nonCheckDisabled}
              <br />
              {checkedDisabled}
            </div>
          </p>
        </div>
      )}
      <br />
      <Typography
        variant="h6"
        component="div"
        style={{ display: "flex", position: "relative" }}
        role="heading"
        aria-level={6}
        className="titleHeading"
      >
        Label
      </Typography>
      <Typography component="p">
        You can provide a label to the Checkbox
      </Typography>
      <br />
      <p
        style={{
          height: "auto",
          width: "auto",
          borderRadius: 6.5,
          border: "2px solid grey",
          alignItems: "center",
          display: "flex",
          justifyContent: "center",
          position: "relative",
        }}
      >
        <div
          style={{
            fontFamily: "Helvetica Neue LT Pro",
            fontStyle: "normal",
            fontWeight: 500,
            fontSize: 16,
            padding: "20px",
          }}
        >
          <CheckBox
            backgroundColor="primary"
            isChecked
            label="Label"
            size="small"
          />
          <br />
          <CheckBox
            backgroundColor="primary"
            disabled
            label="Disabled"
            size="small"
          />
          <div className="show">
            <button
              onClick={() => setShowCheckboxLabel(!showCheckboxLabel)}
              className="btn"
            >
              {showCheckboxLabel ? "hide code" : "show code"}
            </button>
          </div>
        </div>
      </p>
      {showCheckboxLabel && (
        <div>
          <p
            style={{
              background: "#2D2D2D",
              height: "auto",
              width: "auto",
              borderRadius: 11,
              boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
            }}
          >
            <div
              style={{
                fontFamily: "Helvetica Neue LT Pro",
                fontStyle: "normal",
                fontWeight: 500,
                fontSize: 16,
                color: "#E1C16E",
                padding: "20px",
                whiteSpace: "break-spaces",
              }}
            >
              {checkedLabel}
              <br />
              {disabledLabel}
            </div>
          </p>
        </div>
      )}
      <br />
      <Typography
        variant="h6"
        component="div"
        style={{ display: "flex", position: "relative" }}
        role="heading"
        aria-level={6}
        className="titleHeading"
      >
        Size
      </Typography>
      <Typography component="p">
        Use the size prop to change the size of the checkboxes.
      </Typography>
      <br />
      <p
        style={{
          height: "auto",
          width: "auto",
          borderRadius: 6.5,
          border: "2px solid grey",
          alignItems: "center",
          display: "flex",
          justifyContent: "center",
          position: "relative",
        }}
      >
        <div
          style={{
            fontFamily: "Helvetica Neue LT Pro",
            fontStyle: "normal",
            fontWeight: 500,
            fontSize: 16,
            padding: "20px",
          }}
        >
          <CheckBox
            backgroundColor="primary"
            isChecked
            label="Small"
            size="small"
          />
          <br />
          <CheckBox
            backgroundColor="primary"
            isChecked
            label="Medium"
            size="medium"
          />
          <br />
          <CheckBox
            backgroundColor="primary"
            isChecked
            label="Large"
            size="large"
          />
          <div className="show">
            <button
              onClick={() => setShowCheckboxSize(!showCheckboxSize)}
              className="btn"
            >
              {showCheckboxSize ? "hide code" : "show code"}
            </button>
          </div>
        </div>
      </p>
      {showCheckboxSize && (
        <div>
          <p
            style={{
              background: "#2D2D2D",
              height: "auto",
              width: "auto",
              borderRadius: 11,
              boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
            }}
          >
            <div
              style={{
                fontFamily: "Helvetica Neue LT Pro",
                fontStyle: "normal",
                fontWeight: 500,
                fontSize: 16,
                color: "#E1C16E",
                padding: "20px",
                whiteSpace: "break-spaces",
              }}
            >
              {smallCheckbox}
              <br />
              {mediumCheckbox}
              <br />
              {largeCheckbox}
            </div>
          </p>
        </div>
      )}
      <br />
      <Typography
        variant="h6"
        component="div"
        style={{ display: "flex", position: "relative" }}
        role="heading"
        aria-level={6}
        className="titleHeading"
      >
        Color
      </Typography>
      <Typography component="p">
        Use the color prop to change the color of the checkboxes.
      </Typography>
      <br />
      <p
        style={{
          height: "auto",
          width: "auto",
          borderRadius: 6.5,
          border: "2px solid grey",
          alignItems: "center",
          display: "flex",
          justifyContent: "center",
          position: "relative",
        }}
      >
        <div
          style={{
            fontFamily: "Helvetica Neue LT Pro",
            fontStyle: "normal",
            fontWeight: 500,
            fontSize: 16,
            padding: "20px",
          }}
        >
          <CheckBox
            backgroundColor="default"
            isChecked
            label="Default"
            size="small"
          />
          <br />
          <CheckBox
            backgroundColor="primary"
            isChecked
            label="Primary"
            size="small"
          />
          <br />
          <CheckBox
            backgroundColor="secondary"
            isChecked
            label="Secondary"
            size="small"
          />
          <br />
          <CheckBox
            backgroundColor="danger"
            isChecked
            label="Danger"
            size="small"
          />
          <div className="show">
            <button
              onClick={() => setShowCheckboxColor(!showCheckboxColor)}
              className="btn"
            >
              {showCheckboxColor ? "hide code" : "show code"}
            </button>
          </div>
        </div>
      </p>
      {showCheckboxColor && (
        <div>
          <p
            style={{
              background: "#2D2D2D",
              height: "auto",
              width: "auto",
              borderRadius: 11,
              boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
            }}
          >
            <div
              style={{
                fontFamily: "Helvetica Neue LT Pro",
                fontStyle: "normal",
                fontWeight: 500,
                fontSize: 16,
                color: "#E1C16E",
                padding: "20px",
                whiteSpace: "break-spaces",
              }}
            >
              {defaultColor}
              <br />
              {primaryColor}
              <br />
              {secondaryColor}
              <br />
              {dangerColor}
            </div>
          </p>
        </div>
      )}
    </div>
  );
}
