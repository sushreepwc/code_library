import { Route, Routes } from "react-router-dom";
import * as React from "react";
import {
  styled,
  createTheme,
  ThemeProvider,
  useTheme,
} from "@mui/material/styles";
import Box from "@mui/material/Box";
import Drawer from "@mui/material/Drawer";
import CssBaseline from "@mui/material/CssBaseline";
import MuiAppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import List from "@mui/material/List";
import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import Container from "@mui/material/Container";
import MainListItems from "../Dashboard/listItems";
import Dashboard from "../Dashboard/Dashboard";
import Datepicker from "../Datepicker/Datepicker";
import Dropdown from "../Dropdown/Dropdown";
import MaterialUI from "../MaterialUI/MaterialUI";
import Accordion from "../Accordion/accordion";
import ChartAndGraph from "../ChartGraph/ChartGraph";
import AutoCompleteTextfield from "../AutoCompleteTextfield/autocompletetextfield";
import PaginatorPage from "../Paginator/paginatorpage";
import ButtonPage from "../ButtonComponent/button";
import RatingPage from "../RatingComponent/rating";
import SwitchPage from "../SwitchComponent/switch";
import CheckboxPage from "../CheckboxComponent/checkbox";
import AlertPage from "../AlertComponent/Alert";
import TypographyPage from "../Typography/Typography";
import TextField from "../TextField/TextField";
import BrandLogo from "../Assets/pwc-logo.png";
import CodeLibraryLogo from "../Assets/code-library.png";
import LogoDivider from "../Assets/logo-divider.png";
import GridPage from "../GridComponent/Grid";
import BoxPage from "../BoxComponent/Box";

const drawerWidth = 240;

const Main = styled("main", { shouldForwardProp: (prop) => prop !== "open" })(
  ({ theme, open }) => ({
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
      overflow: "hidden",
    }),
    marginLeft: `-${drawerWidth}px`,
    ...(open && {
      transition: theme.transitions.create("margin", {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
      marginLeft: 0,
    }),
  })
);

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
  transition: theme.transitions.create(["margin", "width"], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: `${drawerWidth}px`,
    transition: theme.transitions.create(["margin", "width"], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const DrawerHeader = styled("div")(({ theme }) => ({
  display: "flex",
  alignItems: "center",
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
  justifyContent: "flex-end",
}));

const mdTheme = createTheme();

export default function PersistentDrawerLeft() {
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <ThemeProvider theme={mdTheme}>
      <Box sx={{ display: "flex" }}>
        <CssBaseline />
        <AppBar position="fixed" open={open}>
          <Toolbar
            sx={{
              pr: "24px", // keep right padding when drawer closed
            }}
          >
            <IconButton
              color="inherit"
              aria-label="open drawer"
              onClick={handleDrawerOpen}
              edge="start"
              sx={{
                marginRight: 3,
                color: "black",
                width: 18,
                ...(open && { display: "none" }),
              }}
            >
              <MenuIcon />
            </IconButton>
            <Box
              component="img"
              sx={{
                height: 20,
              }}
              alt="logo"
              src={BrandLogo}
            />

            <Box
              component="img"
              sx={{
                height: 20,
              }}
              alt="logo"
              src={LogoDivider}
            />

            <Box
              component="img"
              sx={{
                height: 32,
                marginLeft: 2,
              }}
              alt="logo"
              src={CodeLibraryLogo}
            />
          </Toolbar>
        </AppBar>
        <Drawer
          sx={{
            width: drawerWidth,
            flexShrink: 0,
            "& .MuiDrawer-paper": {
              width: drawerWidth,
              boxSizing: "border-box",
            },
          }}
          variant="persistent"
          anchor="left"
          open={open}
        >
          <DrawerHeader>
            <IconButton onClick={handleDrawerClose}>
              {theme.direction === "ltr" ? (
                <ChevronLeftIcon sx={{ color: "white" }} />
              ) : (
                <ChevronRightIcon sx={{ color: "white" }} />
              )}
            </IconButton>
          </DrawerHeader>
          <List component="nav">
            <MainListItems />
          </List>
        </Drawer>
        <Main open={open}>
          <Box
            component="main"
            sx={{
              backgroundColor: (theme) =>
                theme.palette.mode === "light"
                  ? theme.palette.primary
                  : theme.palette.primary,
              flexGrow: 1,
            }}
          >
            <Toolbar />
            <React.Fragment>
              <Container maxWidth="lg">
                <Routes>
                  <Route index element={<Dashboard />} />
                  <Route path="/date" element={<Datepicker />} />
                  <Route path="/dropdown" element={<Dropdown />} />
                  <Route path="/materialui" element={<MaterialUI />} />
                  <Route path="/accordion" element={<Accordion />} />
                  <Route path="/chart" element={<ChartAndGraph />} />
                  <Route path="/button" element={<ButtonPage />} />
                  <Route path="/alert" element={<AlertPage />} />
                  <Route path="/typography" element={<TypographyPage />} />
                  <Route path="/rating" element={<RatingPage />} />
                  <Route path="/switch" element={<SwitchPage />} />
                  <Route path="/checkbox" element={<CheckboxPage />} />
                  <Route path="/textfield" element={<TextField />} />
                  <Route
                    path="/autocompletetextfield"
                    element={<AutoCompleteTextfield />}
                  />
                  <Route path="/paginator" element={<PaginatorPage />} />
                  <Route path="/grid" element={<GridPage />} />
                  <Route path="/box" element={<BoxPage />} />
                </Routes>
              </Container>
            </React.Fragment>
          </Box>
        </Main>
      </Box>
    </ThemeProvider>
  );
}
