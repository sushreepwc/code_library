import React from "react";
import Typography from "@mui/material/Typography";
import CodePen from "../Icons/codepen.png";
import styled from "styled-components";

const InstallationIcon = styled.img`
  margin-right: 14px;
  width: 40px;
  height: 40px;
`;

const typographyStyle = {
  marginLeft: 10,
  marginTop: 54,
  display: "flex",
  position: "relative",
};

const paragraphStyle = {
  marginTop: 18,
  marginBottom: 37,
  background: "#F4F4F4",
  // height: 72,
  padding: 20,
  borderRadius: 11,
  boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
  fontFamily: "Helvetica Neue LT Pro",
  fontStyle: "normal",
  fontWeight: 500,
  fontSize: 18,
  lineHeight: 2,
  color: "#636161",
  paddingLeft: 38,
}

export const InstallationComponent = ({ installationCommand }) => {
  return (
    <div>
      <Typography variant="h6" component="div" style={typographyStyle} role="heading" aria-level={6} className="titleHeading">
        <InstallationIcon src={CodePen} alt="codePen" /> Installation
      </Typography>
      <p
        style={paragraphStyle}
      >
        {installationCommand}
      </p>
    </div>
  );
};
