import React from "react";
import { Typography, Box, Grid } from "@mui/material/";
import Command from "../Icons/command.png";

export const PackageLayoutComponent = ({ description, snapShotImg }) => {
  return (
    <>
      <Typography variant="h6" component="div" style={{ marginLeft: 10, display: "flex", position: "relative" }} role="heading" aria-level={6} className="titleHeading">
        <img src={Command} alt="Command" width="40px" height="40px" style={{ marginRight: "14px" }} />
        Layout
      </Typography>
      <Typography component="p" className="topHeadingSub" style={{ marginBottom: 50, marginTop: 18, marginLeft: 55 }}>
        {description}
        
      </Typography>
      <Box
        style={{
          marginBottom: 75,
          marginLeft: 55,
          backgroundColor: "#F3F3F3",
          paddingBottom: 12,
          paddingTop: 12,
          paddingLeft: 12,
          paddingRight: 12,
        }}
        data-testid="autocomplete-image"
      >
        <Grid container>
          <Grid item xs={12}>
            {/* width=855 */}
            <img src={snapShotImg} alt="Snapshot" height="100" width="100%" />
          </Grid>
        </Grid>
      </Box>
    </>
  );
};
