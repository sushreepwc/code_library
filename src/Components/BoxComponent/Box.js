import React from "react";
import Typography from "@mui/material/Typography";
import CodePen from "../Icons/codepen.png";
import Award from "../Icons/award.png";
import { Grid, Box } from "pwc-react-library";

const boxImport = `import { Box } from "pwc-react-library";`;
const basicBox = `
<Box component="div" style={{backgroundColor: "#47b0b0",color: "#ede9f9",borderRadius: "4px"}}>Div</Box>

<Box component="span" style={{backgroundColor: "#47b0b0",color: "#ede9f9",borderRadius: "4px"}}>Span</Box>`;

const BoxComponent = () => {
  const [showBasicBox, setShowBasicBox] = React.useState(false);

  return (
    <div>
      <Typography
        variant="h5"
        component="h5"
        role="heading"
        aria-level={5}
        className="topHeading"
      >
        Box
      </Typography>
      <Typography component="p" className="topHeadingSub">
        The Box component serves as a wrapper component for most of the CSS
        utility needs.
      </Typography>
      <Typography
        variant="h6"
        component="div"
        style={{
          marginTop: 30,
          display: "flex",
          position: "relative",
        }}
        role="heading"
        aria-level={6}
        className="titleHeading"
        gutterBottom
      >
        <img
          src={CodePen}
          alt="codePen"
          width="40px"
          height="40px"
          style={{ marginRight: "14px" }}
        />{" "}
        Import
      </Typography>
      <p
        style={{
          background: "#2D2D2D",
          height: "auto",
          width: "auto",
          borderRadius: 11,
          boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
        }}
      >
        <div
          style={{
            fontFamily: "Helvetica Neue LT Pro",
            fontStyle: "normal",
            fontWeight: 500,
            fontSize: 16,
            color: "#E1C16E",
            padding: "20px",
            whiteSpace: "break-spaces",
          }}
        >
          {boxImport}
        </div>
      </p>
      <Typography
        variant="h6"
        component="div"
        style={{ display: "flex", position: "relative" }}
        role="heading"
        aria-level={6}
        className="titleHeading"
      >
        <img
          src={Award}
          alt="Award"
          width="40px"
          height="40px"
          style={{ marginRight: "14px" }}
        />
        Basic box
      </Typography>
      <Typography
        component="p"
        style={{ marginBottom: 30, marginLeft: 57 }}
        className="topHeadingSub"
      >
        The Box component wraps your component. It creates a new DOM element,a{" "}
        <strong>div</strong> tag that by default can be changed with the
        component prop. Let's say you want to use a <strong>span</strong> tag
        instead:
      </Typography>
      <p
        style={{
          height: "auto",
          width: "auto",
          borderRadius: 6.5,
          border: "2px solid grey",
          alignItems: "center",
          display: "flex",
          justifyContent: "center",
          position: "relative",
        }}
      >
        <div
          style={{
            fontFamily: "Helvetica Neue LT Pro",
            fontStyle: "normal",
            fontWeight: 500,
            fontSize: 16,
            padding: "20px",
          }}
        >
          <Grid container spacing="sm">
            <Grid item lg={8}>
              <Box
                component="div"
                style={{
                  backgroundColor: "#47b0b0",
                  color: "#ede9f9",
                  borderRadius: "4px",
                }}
              >
                Div
              </Box>
            </Grid>

            <Grid item lg={6}>
              <Box
                component="span"
                style={{
                  backgroundColor: "#47b0b0",
                  color: "#ede9f9",
                  borderRadius: "4px",
                }}
              >
                Span
              </Box>
            </Grid>
          </Grid>
          <br />
          <Grid item>
            <div className="show">
              <button
                onClick={() => setShowBasicBox(!showBasicBox)}
                className="btn"
              >
                {showBasicBox ? "hide code" : "show code"}
              </button>
            </div>
          </Grid>
        </div>
      </p>
      {showBasicBox && (
        <div>
          <p
            style={{
              background: "#2D2D2D",
              height: "auto",
              width: "auto",
              borderRadius: 11,
              boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
            }}
          >
            <div
              style={{
                fontFamily: "Helvetica Neue LT Pro",
                fontStyle: "normal",
                fontWeight: 500,
                fontSize: 16,
                color: "#E1C16E",
                padding: "20px",
                whiteSpace: "break-spaces",
              }}
            >
              {basicBox}
            </div>
          </p>
        </div>
      )}
    </div>
  );
};

export default BoxComponent;
