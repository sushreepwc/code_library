import * as React from "react";
import Typography from "@mui/material/Typography";
import CodePen from "../Icons/codepen.png";
import Award from "../Icons/award.png";
import { Rating } from "pwc-react-library";

const ratingImport = `import { Rating } from "pwc-react-library";`;
const ratingbutton = `<Rating number={5} size="medium" defaultValue={2} />`;
const ratingDisabled = `<Rating isDisabled number={5} size="small" />`;
const ratingFeedback = `<Rating feedbacktext number={5} size="small" defaultValue={2}/>`;
const ratingSmall = `<Rating number={5} size="small" defaultValue={2}/>`;
const ratingMedium =`<Rating number={5} size="medium" defaultValue={3}/>`;
const ratingLarge = `<Rating number={5} size="large" defaultValue={4}/>`;

export default function RatingComponent() {
  const [showBasicRating, setShowBasicRating] = React.useState(false);
  const [showHoverFeedback, setShowHoverFeedback] = React.useState(false);
  const [showRatingSizes, setShowRatingSizes] = React.useState(false);


  return (
    <div>
      <Typography
        variant="h5"
        component="h5"
        role="heading"
        aria-level={5}
        className="topHeading"
      >
        Rating
      </Typography>
      <Typography component="p" className="topHeadingSub">
        Ratings provide insight regarding others' opinions and experiences, and
        can allow the user to submit a rating of their own.
      </Typography>
      <Typography
        variant="h6"
        component="div"
        style={{
          marginTop: 30,
          display: "flex",
          position: "relative",
        }}
        role="heading"
        aria-level={6}
        className="titleHeading"
        gutterBottom
      >
        <img
          src={CodePen}
          alt="codePen"
          width="40px"
          height="40px"
          style={{ marginRight: "14px" }}
        />{" "}
        Import
      </Typography>
      <p
        style={{
          background: "#2D2D2D",
          height: "auto",
          width: "auto",
          borderRadius: 11,
          boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
        }}
      >
        <div
          style={{
            fontFamily: "Helvetica Neue LT Pro",
            fontStyle: "normal",
            fontWeight: 500,
            fontSize: 16,
            color: "#E1C16E",
            padding: "20px",
            whiteSpace: "break-spaces",
          }}
        >
          {ratingImport}
        </div>
      </p>
      <Typography
        variant="h6"
        component="div"
        style={{ display: "flex", position: "relative" }}
        role="heading"
        aria-level={6}
        className="titleHeading"
      >
        <img
          src={Award}
          alt="Award"
          width="40px"
          height="40px"
          style={{ marginRight: "14px" }}
        />
        Basic rating
      </Typography>
      <br />
      <p
        style={{
          height: "auto",
          width: "auto",
          borderRadius: 6.5,
          border: "2px solid grey",
          alignItems: "center",
          display: "flex",
          justifyContent: "center",
          position: "relative",
        }}
      >
        <div
          style={{
            fontFamily: "Helvetica Neue LT Pro",
            fontStyle: "normal",
            fontWeight: 500,
            fontSize: 16,
            padding: "20px",
          }}
        >
          <Typography variant="p" component="p">
            No rating given
          </Typography>
          <Rating number={5} size="medium" defaultValue={2} />
          <Typography variant="p" component="p">
            Disabled
          </Typography>
          <Rating isDisabled number={5} size="medium" />
          <div className="show">
            <button
              onClick={() => setShowBasicRating(!showBasicRating)}
              className="btn"
            >
              {showBasicRating ? "hide code" : "show code"}
            </button>
          </div>
        </div>
      </p>
      {showBasicRating && (
        <div>
          <p
            style={{
              background: "#2D2D2D",
              height: "auto",
              width: "auto",
              borderRadius: 11,
              boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
            }}
          >
            <div
              style={{
                fontFamily: "Helvetica Neue LT Pro",
                fontStyle: "normal",
                fontWeight: 500,
                fontSize: 16,
                color: "#E1C16E",
                padding: "20px",
                whiteSpace: "break-spaces",
              }}
            >
              {ratingbutton}
              <br />
              {ratingDisabled}
              <br />
            </div>
          </p>
        </div>
      )}
      <Typography
        variant="h6"
        component="div"
        style={{ display: "flex", position: "relative" }}
        role="heading"
        aria-level={6}
        className="titleHeading"
      >
        Hover feedback
      </Typography>
      <Typography component="p" className="topHeadingSub">
        You can display a label on hover to help the user pick the correct
        rating value.
      </Typography>
      <br />
      <p
        style={{
          height: "auto",
          width: "auto",
          borderRadius: 6.5,
          border: "2px solid grey",
          alignItems: "center",
          display: "flex",
          justifyContent: "center",
          position: "relative",
        }}
      >
        <div
          style={{
            fontFamily: "Helvetica Neue LT Pro",
            fontStyle: "normal",
            fontWeight: 500,
            fontSize: 16,
            padding: "30px",
          }}
        >
          <Rating feedbacktext number={5} size="small" defaultValue={2}/>
          <div className="show">
            <button
              onClick={() => setShowHoverFeedback(!showHoverFeedback)}
              className="btn"
            >
              {showHoverFeedback ? "hide code" : "show code"}
            </button>
          </div>
        </div>
      </p>
      {showHoverFeedback && (
        <div>
          <p
            style={{
              background: "#2D2D2D",
              height: "auto",
              width: "auto",
              borderRadius: 11,
              boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
            }}
          >
            <div
              style={{
                fontFamily: "Helvetica Neue LT Pro",
                fontStyle: "normal",
                fontWeight: 500,
                fontSize: 16,
                color: "#E1C16E",
                padding: "20px",
                whiteSpace: "break-spaces",
              }}
            >
              {ratingFeedback}
              <br />
            </div>
          </p>
        </div>
      )}
      <Typography
        variant="h6"
        component="div"
        style={{ display: "flex", position: "relative" }}
        role="heading"
        aria-level={6}
        className="titleHeading"
      >
        Sizes
      </Typography>
      <Typography component="p" className="topHeadingSub">
        For larger or smaller ratings use the size prop.
      </Typography>
      <br />
      <p
        style={{
          height: "auto",
          width: "auto",
          borderRadius: 6.5,
          border: "2px solid grey",
          alignItems: "center",
          display: "flex",
          justifyContent: "center",
          position: "relative",
        }}
      >
        <div
          style={{
            fontFamily: "Helvetica Neue LT Pro",
            fontStyle: "normal",
            fontWeight: 500,
            fontSize: 16,
            padding: "30px",
          }}
        >
          <Rating number={5} size="small" defaultValue={2}/>
          <Rating number={5} size="medium" defaultValue={3}/>
          <Rating number={5} size="large" defaultValue={4}/>
          <div className="show">
            <button
              onClick={() => setShowRatingSizes(!showRatingSizes)}
              className="btn"
            >
              {showRatingSizes ? "hide code" : "show code"}
            </button>
          </div>
        </div>
      </p>
      {showRatingSizes && (
        <div>
          <p
            style={{
              background: "#2D2D2D",
              height: "auto",
              width: "auto",
              borderRadius: 11,
              boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
            }}
          >
            <div
              style={{
                fontFamily: "Helvetica Neue LT Pro",
                fontStyle: "normal",
                fontWeight: 500,
                fontSize: 16,
                color: "#E1C16E",
                padding: "20px",
                whiteSpace: "break-spaces",
              }}
            >
              {ratingSmall}
              <br />
              {ratingMedium}
              <br />
              {ratingLarge}
              <br />
            </div>
          </p>
        </div>
      )}
    </div>
  );
}
