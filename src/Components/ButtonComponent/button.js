import React from "react";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";
import CodePen from "../Icons/codepen.png";
import Award from "../Icons/award.png";
import { Button } from "pwc-react-library";
import "./button.scss";

const buttonImport = `import { Button } from "pwc-react-library";`;
const buttonText = `<Button shadow={false} text="Text" textColor="primary" type="text" />`;
const buttonSecondary = `<Button backgroundColor="secondary" text="Secondary" />`;
const buttonOutlined = `<Button text="Outline" textColor="primary" type="outline" />`;
const textButton = `<Button shadow={false} text="Text" textColor="primary" type="text" />`;
const textDisabled = `<Button isDisabled shadow={false} text="Disabled" textColor="default" type="text" />`;
const smallSize = `<Button shadow={false} size="small" text="Default" textColor="primary" type="text" />`;
const mediumSize = `<Button shadow={false} size="medium" text="Default" textColor="primary" type="text" />`;
const largeSize = `<Button shadow={false} size="large" text="Default" textColor="primary" type="text" />`;
const smallOutlined = `<Button shadow={false} size="small" text="Default" textColor="primary" type="outline" />`;
const mediumOutlined = `<Button shadow={false} size="medium" text="Default" textColor="primary" type="outline" />`;
const largeOutlined = `<Button shadow={false} size="large" text="Default" textColor="primary" type="outline" />`;
const smallContained = `<Button backgroundColor="primary" size="small" text="Default" textColor="default" />`;
const mediumContained = `<Button backgroundColor="primary" size="medium" text="Default" textColor="default" />`;
const largeContained = `<Button backgroundColor="primary" size="large" text="Default" textColor="default" />`;
const startIconbutton = `<Button backgroundColor="primary" startIcon text="Default" />`;
const endIconbutton = `<Button backgroundColor="primary" endIcon text="Default" />`;
const buttonDanger = `<Button backgroundColor="danger" text="Danger" />`;

const ButtonComponent = () => {
  const [showBasicButtons, setShowBasicButtons] = React.useState(false);
  const [showTextButtons, setShowTextButtons] = React.useState(false);
  const [showSizeButtons, setShowSizeButtons] = React.useState(false);
  const [showIconButtons, setShowIconButtons] = React.useState(false);

  return (
    <div>
      <Typography
        variant="h5"
        component="h5"
        role="heading"
        aria-level={5}
        className="topHeading"
      >
        Button
      </Typography>
      <Typography component="p" className="topHeadingSub">
        Buttons allow users to take actions, and make choices, with a single
        tap.
      </Typography>
      <Typography
        variant="h6"
        component="div"
        style={{
          marginTop: 30,
          display: "flex",
          position: "relative",
        }}
        role="heading"
        aria-level={6}
        className="titleHeading"
        gutterBottom
      >
        <img
          src={CodePen}
          alt="codePen"
          width="40px"
          height="40px"
          style={{ marginRight: "14px" }}
        />{" "}
        Import
      </Typography>
      <p
        style={{
          background: "#2D2D2D",
          height: "auto",
          width: "auto",
          borderRadius: 11,
          boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
        }}
      >
        <div
          style={{
            fontFamily: "Helvetica Neue LT Pro",
            fontStyle: "normal",
            fontWeight: 500,
            fontSize: 16,
            color: "#E1C16E",
            padding: "20px",
            whiteSpace: "break-spaces",
          }}
        >
          {buttonImport}
        </div>
      </p>
      <Typography
        variant="h6"
        component="div"
        style={{ display: "flex", position: "relative" }}
        role="heading"
        aria-level={6}
        className="titleHeading"
      >
        <img
          src={Award}
          alt="Award"
          width="40px"
          height="40px"
          style={{ marginRight: "14px" }}
        />
        Basic button
      </Typography>
      <Typography
        component="p"
        style={{ marginBottom: 30, marginLeft: 57 }}
        className="topHeadingSub"
      >
        The Button comes with three variants: text, secondary, outlined, danger.
      </Typography>
      <p
        style={{
          height: "auto",
          width: "auto",
          borderRadius: 6.5,
          border: "2px solid grey",
          alignItems: "center",
          display: "flex",
          justifyContent: "center",
          position: "relative",
        }}
      >
        <div
          style={{
            fontFamily: "Helvetica Neue LT Pro",
            fontStyle: "normal",
            fontWeight: 500,
            fontSize: 16,
            padding: "20px",
          }}
        >
          <Grid container spacing={2}>
            <Grid item>
              <Button backgroundColor="secondary" text="Primary" />
            </Grid>
            <Grid item>
              <Button text="Secondary" textColor="primary" type="outline" />
            </Grid>
            <Grid item>
              <Button backgroundColor="danger" text="Danger" />
            </Grid>
            <Grid item>
              <Button
                shadow={false}
                text="Text"
                textColor="primary"
                type="text"
              />
            </Grid>
            <br />
            <Grid item>
              <div className="show">
                <button
                  onClick={() => setShowBasicButtons(!showBasicButtons)}
                  className="btn"
                >
                  {showBasicButtons ? "hide code" : "show code"}
                </button>
              </div>
            </Grid>
          </Grid>
        </div>
      </p>
      {showBasicButtons && (
        <div>
          <p
            style={{
              background: "#2D2D2D",
              height: "auto",
              width: "auto",
              borderRadius: 11,
              boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
            }}
          >
            <div
              style={{
                fontFamily: "Helvetica Neue LT Pro",
                fontStyle: "normal",
                fontWeight: 500,
                fontSize: 16,
                color: "#E1C16E",
                padding: "20px",
                whiteSpace: "break-spaces",
              }}
            >
              {buttonText}
              <br />
              {buttonSecondary}
              <br />
              {buttonOutlined}
              <br />
              {buttonDanger}
            </div>
          </p>
        </div>
      )}
      <br />
      <Typography
        variant="h6"
        component="div"
        style={{ display: "flex", position: "relative" }}
        role="heading"
        aria-level={6}
        className="titleHeading"
        gutterBottom
      >
        Text button
      </Typography>
      <Typography
        component="p"
        style={{ marginBottom: 30 }}
        className="topHeadingSub"
        gutterBottom
      >
        Text buttons are typically used for less-pronounced actions, including
        those located: in dialogs, in cards. In cards, text buttons help
        maintain an emphasis on card content.
      </Typography>
      <p
        style={{
          height: "auto",
          width: "auto",
          borderRadius: 6.5,
          border: "2px solid grey",
          alignItems: "center",
          display: "flex",
          justifyContent: "center",
          position: "relative",
        }}
      >
        <div
          style={{
            fontFamily: "Helvetica Neue LT Pro",
            fontStyle: "normal",
            fontWeight: 500,
            fontSize: 16,
            padding: "20px",
          }}
        >
          <Grid container spacing={2}>
            <Grid item>
              <Button
                shadow={false}
                text="Text"
                textColor="primary"
                type="text"
              />
            </Grid>
            <Grid item>
              <Button
                isDisabled
                shadow={false}
                text="Disabled"
                textColor="default"
                type="text"
              />
            </Grid>
          </Grid>
          <br />
          <Grid item>
            <div className="show">
              <button
                onClick={() => setShowTextButtons(!showTextButtons)}
                className="btn"
              >
                {showTextButtons ? "hide code" : "show code"}
              </button>
            </div>
          </Grid>
        </div>
      </p>
      <br />
      {showTextButtons && (
        <p
          style={{
            background: "#2D2D2D",
            height: "auto",
            width: "auto",
            borderRadius: 11,
            boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
          }}
        >
          <div
            style={{
              fontFamily: "Helvetica Neue LT Pro",
              fontStyle: "normal",
              fontWeight: 500,
              fontSize: 16,
              color: "#E1C16E",
              padding: "20px",
              whiteSpace: "break-spaces",
            }}
          >
            {textButton}
            <br />
            {textDisabled}
          </div>
        </p>
      )}
      <br />
      <Typography
        variant="h6"
        component="div"
        style={{ display: "flex", position: "relative" }}
        role="heading"
        aria-level={6}
        className="titleHeading"
        gutterBottom
      >
        Sizes
      </Typography>
      <Typography
        component="p"
        style={{ marginBottom: 30 }}
        className="topHeadingSub"
        gutterBottom
      >
        For larger or smaller buttons, use the size prop.
      </Typography>
      <p
        style={{
          height: "auto",
          width: "auto",
          borderRadius: 6.5,
          border: "2px solid grey",
          alignItems: "center",
          display: "flex",
          justifyContent: "center",
          position: "relative",
        }}
      >
        <div
          style={{
            fontFamily: "Helvetica Neue LT Pro",
            fontStyle: "normal",
            fontWeight: 500,
            fontSize: 16,
            padding: "20px",
          }}
        >
          <Grid container spacing={2}>
            <Grid item>
              <Button
                shadow={false}
                size="small"
                text="Small"
                textColor="primary"
                type="text"
              />
            </Grid>
            <Grid item>
              <Button
                shadow={false}
                size="medium"
                text="Medium"
                textColor="primary"
                type="text"
              />
            </Grid>
            <Grid item>
              <Button
                shadow={false}
                size="large"
                text="Large"
                textColor="primary"
                type="text"
              />
            </Grid>
          </Grid>
          <br />
          <Grid container spacing={2}>
            <Grid item>
              <Button
                shadow={false}
                size="small"
                text="Small"
                textColor="primary"
                type="outline"
              />
            </Grid>
            <Grid item>
              <Button
                shadow={false}
                size="medium"
                text="Medium"
                textColor="primary"
                type="outline"
              />
            </Grid>
            <Grid item>
              <Button
                shadow={false}
                size="large"
                text="Large"
                textColor="primary"
                type="outline"
              />
            </Grid>
          </Grid>
          <br />
          <Grid container spacing={2} marginLeft="2px">
            <Grid>
              <Button backgroundColor="primary" size="small" text="Small" />
            </Grid>
            <Grid item>
              <Button backgroundColor="primary" size="medium" text="Medium" />
            </Grid>
            <Grid item>
              <Button backgroundColor="primary" size="large" text="Large" />
            </Grid>
          </Grid>
          <br />
          <Grid item>
            <div className="show">
              <button
                onClick={() => setShowSizeButtons(!showSizeButtons)}
                className="btn"
              >
                {showSizeButtons ? "hide code" : "show code"}
              </button>
            </div>
          </Grid>
        </div>
      </p>
      <br />
      {showSizeButtons && (
        <p
          style={{
            background: "#2D2D2D",
            height: "auto",
            width: "auto",
            borderRadius: 11,
            boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
          }}
        >
          <div
            style={{
              fontFamily: "Helvetica Neue LT Pro",
              fontStyle: "normal",
              fontWeight: 500,
              fontSize: 16,
              color: "#E1C16E",
              padding: "20px",
              whiteSpace: "break-spaces",
            }}
          >
            {smallSize}
            <br />
            {mediumSize}
            <br />
            {largeSize}
            <br />
            {smallOutlined}
            <br />
            {mediumOutlined}
            <br />
            {largeOutlined}
            <br />
            {smallContained}
            <br />
            {mediumContained}
            <br />
            {largeContained}
            <br />
          </div>
        </p>
      )}
      <br />
      <Typography
        variant="h6"
        component="div"
        style={{ display: "flex", position: "relative" }}
        role="heading"
        aria-level={6}
        className="titleHeading"
        gutterBottom
      >
        Icon buttons
      </Typography>
      <Typography
        component="p"
        style={{ marginBottom: 30 }}
        className="topHeadingSub"
        gutterBottom
      >
        Icon buttons are commonly found in app bars and toolbars. Icons are also
        appropriate for toggle buttons that allow a single choice to be selected
        or deselected, such as adding or removing a star to an item.
      </Typography>
      <p
        style={{
          height: "auto",
          width: "auto",
          borderRadius: 6.5,
          border: "2px solid grey",
          display: "flex",
          justifyContent: "center",
          position: "relative",
        }}
      >
        <div
          style={{
            fontFamily: "Helvetica Neue LT Pro",
            fontStyle: "normal",
            fontWeight: 500,
            fontSize: 16,
            padding: "20px",
          }}
        >
          <Grid container spacing={2}>
            <Grid item>
              <Button backgroundColor="primary" startIcon text="Default" />
            </Grid>
            <Grid item>
              <Button backgroundColor="primary" endIcon text="Default" />
            </Grid>
          </Grid>
          <br />
          <Grid item>
            <div className="show">
              <button
                onClick={() => setShowIconButtons(!showIconButtons)}
                className="btn"
              >
                {showIconButtons ? "hide code" : "show code"}
              </button>
            </div>
          </Grid>
        </div>
      </p>
      <br />
      {showIconButtons && (
        <p
          style={{
            background: "#2D2D2D",
            height: "auto",
            width: "auto",
            borderRadius: 11,
            boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
          }}
        >
          <div
            style={{
              fontFamily: "Helvetica Neue LT Pro",
              fontStyle: "normal",
              fontWeight: 500,
              fontSize: 16,
              color: "#E1C16E",
              padding: "20px",
              whiteSpace: "break-spaces",
            }}
          >
            {startIconbutton}
            <br />
            {endIconbutton}
          </div>
        </p>
      )}
    </div>
  );
};
export default ButtonComponent;
