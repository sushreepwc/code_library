import React from "react";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import { CardContent } from "@mui/material";
import Card from "@mui/material/Card";

import CodePen from "../Icons/codepen.png";
import Command from "../Icons/command.png";
import DropdownOutput from "../Assets/dropdown.png";
import Award from "../Icons/award.png";
import CPU from "../Icons/cpu.png";

import { CascadedSelectBox } from "code-library-dropdown/dist/components";
import Data from "../../Data/DropdownData";

const Dropdown = () => {
  return (
    <div>
      <Typography
        variant="h5"
        component="h5"
        role="heading"
        aria-level={5}
        className="topHeading"
      >
        Dropdown
      </Typography>
      <Typography component="p" className="topHeadingSub">
        A React component which provides multi select functionality with various
        features like selection limit, CSS customization, checkbox, search
        option, disable preselected values, flat array, keyboard navigation for
        accessibility and grouping features. Also it has feature to behave like
        normal dropdown(means single select dropdown).
      </Typography>
      <Typography
        variant="h6"
        component="div"
        style={{
          marginLeft: 10,
          marginTop: 54,
          display: "flex",
          position: "relative",
        }}
        role="heading"
        aria-level={6}
        className="titleHeading"
      >
        <img
          src={CodePen}
          alt="codePen"
          width="40px"
          height="40px"
          style={{ marginRight: "14px" }}
        />{" "}
        Installation
      </Typography>
      <p
        style={{
          marginTop: 18,
          marginBottom: 37,
          background: "#F4F4F4",
          height: 72,
          padding: 20,
          borderRadius: 11,
          boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
          fontFamily: "Helvetica Neue LT Pro",
          fontStyle: "normal",
          fontWeight: 500,
          fontSize: 18,
          lineHeight: 2,
          color: "#636161",
          paddingLeft: 38,
        }}
      >
        npm i code-library-dropdown
      </p>
      <Typography
        variant="h6"
        component="div"
        style={{ marginLeft: 10, display: "flex", position: "relative" }}
        role="heading"
        aria-level={6}
        className="titleHeading"
      >
        <img
          src={Command}
          alt="Command"
          width="40px"
          height="40px"
          style={{ marginRight: "14px" }}
        />
        Layout
      </Typography>
      <Typography
        component="p"
        className="topHeadingSub"
        style={{ marginBottom: 50, marginTop: 18, marginLeft: 55 }}
      >
        The widget is useful for dropdown select option. The value of field will
        select from options.
      </Typography>
      <Box
        style={{
          marginBottom: 75,
          marginLeft: 55,
          backgroundColor: "#F3F3F3",
          paddingBottom: 12,
          paddingTop: 12,
          paddingLeft: 12,
        }}
        data-testid="autocomplete-image"
      >
        <Grid container>
          <Grid item xs={5}>
            <img src={DropdownOutput} alt="DropDown" height="100" width="855" />
          </Grid>
        </Grid>
      </Box>

      <Box>
        <Grid container spacing={4}>
          <Grid item xs={6}>
            <Typography
              variant="h6"
              component="div"
              style={{ marginLeft: 10, display: "flex", position: "relative" }}
              role="heading"
              aria-level={6}
              className="titleHeading"
            >
              <img
                src={Award}
                alt="Award"
                width="40px"
                height="40px"
                style={{ marginRight: "14px" }}
              />
              Default View
            </Typography>
            <Typography
              component="p"
              style={{ marginBottom: 30, marginLeft: 67 }}
              className="topHeadingSub"
            >
              The user can on select field and choose the option from options
              dropdown.
            </Typography>
            <Card
              style={{
                background: "#F4F4F4",
                height: 301,
                width: 445,
                borderRadius: 11,
                boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
              }}
            >
              <CardContent>
                <div
                  style={{
                    fontFamily: "Helvetica Neue LT Pro",
                    fontStyle: "normal",
                    fontWeight: 500,
                    fontSize: 16,
                    // lineHeight: 5,
                    color: "#636161",
                    marginTop: "70px",
                    // paddingTop: 30,
                    // paddingLeft: 55,
                  }}
                >
                  <CascadedSelectBox options={Data.DropDownOptions} />
                </div>
              </CardContent>
            </Card>
          </Grid>

          <Grid item xs={6}>
            <Typography
              variant="h6"
              component="div"
              style={{ marginLeft: 10, display: "flex", position: "relative" }}
              role="heading"
              aria-level={6}
              className="titleHeading"
            >
              <img
                src={CPU}
                alt="Award"
                width="40px"
                height="40px"
                style={{ marginRight: "14px" }}
              />
              Usage (Code View)
            </Typography>
            <Typography
              component="p"
              style={{ marginBottom: 55, marginLeft: 67 }}
              className="topHeadingSub"
            >
              Usage
            </Typography>
            <p
              style={{
                background: "#2D2D2D",
                height: 301,
                width: 445,
                borderRadius: 11,
                boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
                overflowY: "scroll",
              }}
            >
              <div
                style={{
                  fontFamily: "Helvetica Neue LT Pro",
                  fontStyle: "normal",
                  fontWeight: 500,
                  fontSize: 16,
                  color: "#FFFFFF",
                  padding: "26px 24px 16px 50px",
                  whiteSpace: "break-spaces",
                }}
              >
                {Data.DropDownCode}
              </div>
            </p>
          </Grid>
        </Grid>
      </Box>
    </div>
  );
};

export default Dropdown;
