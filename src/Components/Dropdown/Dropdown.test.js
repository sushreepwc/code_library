import React from "react";
import { fireEvent, render, screen } from "@testing-library/react";
import Dropdown from "./Dropdown";

describe("Dropdown", () => {
    it("onload select field is closed", () => {
        render(<Dropdown />);
        const container = document.getElementsByClassName("cascadedDiv")[0];
        const container2 = container.getElementsByTagName("div")[0];
        expect(container2.classList.contains('ant-select-open')).toBe(false)
    });

    it("onclick select option is opened", () => {
        render(<Dropdown />);
        const container = document.getElementsByClassName("cascadedDiv")[0];
        const container2 = container.getElementsByTagName("div")[0];
        fireEvent.click(container2)
        setTimeout(() => {
            expect(container2.classList.contains('ant-select-open')).toBe(true)    
        }, 3000);
    });

    it("select option by clicking on checkbox", () => {
        render(<Dropdown />);
        const container = document.getElementsByClassName("cascadedDiv")[0];
        const container2 = container.getElementsByTagName("div")[0];
        fireEvent.click(container2)

        
        setTimeout(() => {
            expect(container2.classList.contains('ant-select-open')).toBe(true)    
            const checkboxOption = screen.getByTitle('Light')
            fireEvent.click(checkboxOption);
            expect(document.getElementsByTagName('span')[0].contains('ant-select-selection-item')).toBe(true)    
        }, 3000);

    });

    it("default width 100%", () => {
        render(<Dropdown />);
        const container = document.getElementsByClassName("cascadedDiv")[0];
        const container2 = container.getElementsByTagName("div")[0];
        expect(container2.style.width).toBe("100%")
    });



})