import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import accordionOpen from "../Assets/accordionOpen.png";
import accordionClosed from "../Assets/accordionClosed.png";
import { CardContent } from "@mui/material";
import Card from "@mui/material/Card";
import Data from "../../Data/AccordionData";
import { AccordionComponent } from "react_accordion_pwc";
import CodePen from "../Icons/codepen.png";
import Command from "../Icons/command.png";
import Award from "../Icons/award.png";
import CPU from "../Icons/cpu.png";

const accordionImgOpen = "accordion-image-open";
const accordionImgClosed = "accodrion-image-closed";

const Accordion = () => {
  return (
    <div>
      <Typography
        variant="h5"
        component="h5"
        role="heading"
        aria-level={5}
        className="topHeading"
      >
        Accordion
      </Typography>
      <Typography component="p" className="topHeadingSub">
        Accordion lists can be added and managed by adding the Accordion item to
        your column through the Component Type menu.
      </Typography>
      <Typography
        variant="h6"
        component="div"
        style={{
          marginLeft: 10,
          marginTop: 54,
          display: "flex",
          position: "relative",
        }}
        role="heading"
        aria-level={6}
        className="titleHeading"
      >
        <img
          src={CodePen}
          alt="codePen"
          width="40px"
          height="40px"
          style={{ marginRight: "14px" }}
        />{" "}
        Installation
      </Typography>
      <p
        style={{
          marginTop: 18,
          marginBottom: 37,
          background: "#F4F4F4",
          height: 72,
          padding: 20,
          borderRadius: 11,
          boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
          fontFamily: "Helvetica Neue LT Pro",
          fontStyle: "normal",
          fontWeight: 500,
          fontSize: 18,
          lineHeight: 2,
          color: "#636161",
          paddingLeft: 38,
        }}
      >
        npm i react_accordion_pwc
      </p>
      <Typography
        variant="h6"
        component="div"
        style={{ marginLeft: 10, display: "flex", position: "relative" }}
        role="heading"
        aria-level={6}
        className="titleHeading"
      >
        <img
          src={Command}
          alt="Command"
          width="40px"
          height="40px"
          style={{ marginRight: "14px" }}
        />
        Layout
      </Typography>
      <Typography
        component="p"
        className="topHeadingSub"
        style={{ marginBottom: 50, marginTop: 18, marginLeft: 55 }}
      >
        Cascading dropdown is a group of dropdowns where the value of one
        dropdown depends upon another dropdown value. Child dropdown values are
        populated based on the item selected in the parent dropdown. We should
        have basic knowledge of React. js and Web API.
      </Typography>
      <Box
        style={{
          marginBottom: 75,
          marginLeft: 55,
          backgroundColor: "#F3F3F3",
          paddingBottom: 12,
          paddingTop: 12,
          paddingLeft: 12,
        }}
        data-testid="autocomplete-image"
      >
        <Grid container style={{ paddingLeft: 155, paddingBottom: 50 }}>
          <Grid item xs={6}>
            <img
              src={accordionOpen}
              alt="accordionOpen"
              height="200"
              width="500"
              data-testid={accordionImgOpen}
            />
          </Grid>
          </Grid>
          <Grid container style={{ paddingLeft: 30 }}>
          <Grid item xs={6}>
            <img
              src={accordionClosed}
              alt="accordionClosed"
              height="40"
              width="800"
              data-testid={accordionImgClosed}
            />
          </Grid>
        </Grid>
      </Box>

      <Box>
        <Grid container spacing={4}>
          <Grid item xs={6}>
            <Typography
              variant="h6"
              component="div"
              style={{ marginLeft: 10, display: "flex", position: "relative" }}
              role="heading"
              aria-level={6}
              className="titleHeading"
            >
              <img
                src={Award}
                alt="Award"
                width="40px"
                height="40px"
                style={{ marginRight: "14px" }}
              />
              Default View
            </Typography>
            <Typography
              component="p"
              style={{ marginBottom: 55, marginLeft: 67 }}
              className="topHeadingSub"
            >
              The user can click on the arrow to view the data.
            </Typography>
            <Card
              style={{
                background: "#F4F4F4",
                width: "auto",
                height: "auto",
                borderRadius: 11,
                boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
              }}
            >
              <CardContent>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center",
                    fontFamily: "Helvetica Neue LT Pro",
                    fontStyle: "normal",
                    fontWeight: 500,
                    fontSize: 16,
                    color: "#636161",
                  }}
                >
                  <AccordionComponent
                    title="Custom accordion open state"
                    defaultOpen={true}
                  >
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry. Lorem Ipsum has been the industry's
                    standard dummy text ever since the 1500s, when an unknown
                    printer took a galley of type and scrambled it to make a
                    type specimen book
                  </AccordionComponent>
                  <AccordionComponent
                    title="Custom accordion closed state"
                    defaultOpen={false}
                    fullWidth
                  >
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry. Lorem Ipsum has been the industry's
                    standard dummy text ever since the 1500s, when an unknown
                    printer took a galley of type and scrambled it to make a
                    type specimen book
                  </AccordionComponent>
                </div>
              </CardContent>
            </Card>
          </Grid>
          <Grid item xs={6}>
            <Typography
              variant="h6"
              component="div"
              style={{ marginLeft: 10, display: "flex", position: "relative" }}
              role="heading"
              aria-level={6}
              className="titleHeading"
            >
              <img
                src={CPU}
                alt="Award"
                width="40px"
                height="40px"
                style={{ marginRight: "14px" }}
              />
              Usage (Code View)
            </Typography>
            <Typography
              component="p"
              style={{ marginBottom: 55, marginLeft: 67 }}
              className="topHeadingSub"
            >
              Usage
            </Typography>
            <p
              style={{
                background: "#2D2D2D",
                height: 328,
                width: 445,
                borderRadius: 11,
                boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
                overflowY: 'scroll'
              }}
            >
              <div
                style={{
                  fontFamily: "Helvetica Neue LT Pro",
                  fontStyle: "normal",
                  fontWeight: 500,
                  fontSize: 16,
                  color: "#FFFFFF",
                  padding: "26px 24px 16px 50px",
                  whiteSpace: "break-spaces",
                }}
              >
                {Data.AccordionCode}
              </div>
            </p>
          </Grid>
        </Grid>
      </Box>
    </div>
  );
};
export default Accordion;
