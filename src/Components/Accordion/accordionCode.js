import React from "react";
import { AccordionComponent } from "react_accordion_pwc";

const AccordionCode = () => {
  return (
    <div>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <AccordionComponent
          title="Custom accordion open state"
          defaultOpen={true}
        >
          <div>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s, when an unknown printer took a galley of type
            and scrambled it to make a type specimen book
          </div>
        </AccordionComponent>
        <AccordionComponent
          title="Custom accordion closed state"
          defaultOpen={false}
          fullWidth
        >
          <div>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s, when an unknown printer took a galley of type
            and scrambled it to make a type specimen book
          </div>
        </AccordionComponent>
      </div>
    </div>
  );
};

export default AccordionCode;
