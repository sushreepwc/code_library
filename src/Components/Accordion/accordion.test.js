import React from "react";
import { fireEvent, render, screen } from "@testing-library/react";
import Accordion from "./Accordion";

describe("Accordion", () => {
  it("Renders the accordion headings", () => {
    const renderer = render(<Accordion />);
    expect(renderer).toMatchSnapshot();
    screen.getByRole("heading", { level: 5, name: "Accordion" });
    screen.getByRole("heading", { level: 6, name: "Command Layout" });
    screen.getByRole("heading", { level: 6, name: "Award Default View" });
    screen.getByRole("heading", { level: 6, name: "Award Usage (Code View)" });
  });

  it("Renders the accordion text", () => {
    render(<Accordion />);
    screen.getByText(
      "Accordion lists can be added and managed by adding the Accordion item to your column through the Component Type menu."
    );
    screen.getByText(
      "Cascading dropdown is a group of dropdowns where the value of one dropdown depends upon another dropdown value. Child dropdown values are populated based on the item selected in the parent dropdown. We should have basic knowledge of React. js and Web API."
    );
    screen.getByText("The user can click on the arrow to view the data.");
  });

  it("Renders the accordion images ", () => {
    render(<Accordion />);
    screen.getByTestId("accordion-image-open");
    screen.getByTestId("accodrion-image-closed");
  });

  it("Renders AccordionComponent open state", () => {
    render(
      <Accordion title="Custom accordion open state" defaultOpen={true} />
    );
    const expandAccordion = screen.getByText("Custom accordion open state");
    fireEvent.click(expandAccordion);
    expect(expandAccordion).toHaveClass("accordiontitle");
    screen.getAllByText(
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book"
    );
  });

  it("Renders AccordionComponent closed state", () => {
    render(
      <Accordion title="Custom accordion closed state" defaultOpen={false} />
    );
    const collapsedAccordion = screen.getByText(
      "Custom accordion closed state"
    );
    fireEvent.click(collapsedAccordion);
    expect(collapsedAccordion).toHaveClass("accordiontitle open");
    screen.getAllByText(
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book"
    );
  });
});
