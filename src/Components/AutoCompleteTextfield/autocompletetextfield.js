import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import { CardContent } from "@mui/material";
import Card from "@mui/material/Card";
import Cards from "../Cards/Cards";
import AutucompleteTextfield from "../Assets/AutocompleteTextfield.png";
import Autocomplete from "autocomplete-textfield";
import Data from "../../Data/AutocompleteData";
import CodePen from "../Icons/codepen.png";
import Command from "../Icons/command.png";
import Award from "../Icons/award.png";
import CPU from "../Icons/cpu.png";
import Layout from "../Layout/Layout";
import { DescriptionComponent, InstallationComponent, PackageLayoutComponent, DefaultViewComponent, CodeViewComponent } from "../";

import Paper from "@mui/material/Paper";
import { styled } from "@mui/material/styles";

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: "center",
  color: theme.palette.text.secondary,
}));

const AutoCompleteTextfield = () => {
  return (
    <div>
      <Box sx={{ flexGrow: 1 }}>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <DescriptionComponent title={"Autocomplete Textfield"} description={"The AutoComplete component provides the matched suggestion list when type into the input, from which the user can select one."} />
          </Grid>
          <Grid item xs={12}>
            <InstallationComponent installationCommand={"npm i autocomplete-textfield"} />
          </Grid>
          <Grid item xs={12}>
            <PackageLayoutComponent
              snapShotImg={AutucompleteTextfield}
              description={`The widget is useful for setting the value of a single-line textbox in one of two types of scenarios: The value for the textbox must be chosen from a predefined set of allowed values, e.g., a location field must contain a valid location name: combo box. The textbox may contain any arbitrary value, but it is advantageous to suggest possible values to the user, e.g., a search field may suggest similar or previous searches to save the user time: free solo. It's meant to be an improved version of the "react-select" and "downshift" packages.`}
            />
          </Grid>

          <Grid container spacing={4}>
            <Grid item xs={12} md={6}>
              <DefaultViewComponent title={"Default View"} description={"The user can click or type into the input field to get matched suggestion list."}>
                <Card
                  style={{
                    background: "#F4F4F4",
                    height: 301,
                    width: "100%",
                    borderRadius: 11,
                    boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
                  }}
                >
                  <CardContent>
                    <div
                      style={{
                        fontFamily: "Helvetica Neue LT Pro",
                        fontStyle: "normal",
                        fontWeight: 500,
                        fontSize: 16,
                        lineHeight: 5,
                        color: "#636161",
                        paddingTop: 30,
                        paddingLeft: 55,
                      }}
                    >
                      Autocomplete Textfield
                      <Autocomplete data={Data.Countries} />
                    </div>
                  </CardContent>
                </Card>
              </DefaultViewComponent>
            </Grid>
            <Grid item xs={12} md={6}>
              <CodeViewComponent title={"Usage (Code View)"} description={"Usage"}>
                <p
                  style={{
                    background: "#2D2D2D",
                    height: 301,
                    width: "100%",
                    borderRadius: 11,
                    boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
                    overflowY: "scroll",
                  }}
                >
                  <div
                    style={{
                      fontFamily: "Helvetica Neue LT Pro",
                      fontStyle: "normal",
                      fontWeight: 500,
                      fontSize: 16,
                      color: "#FFFFFF",
                      padding: "26px 24px 16px 50px",
                      whiteSpace: "break-spaces",
                    }}
                  >
                    {Data.AutocompleteCode}
                  </div>
                </p>
              </CodeViewComponent>
            </Grid>
          </Grid>
        </Grid>
      </Box>
    </div>
  );
};
export default AutoCompleteTextfield;
