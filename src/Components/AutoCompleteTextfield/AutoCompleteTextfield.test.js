import { render, screen, fireEvent, within } from '@testing-library/react';
import AutoCompleteTextfield from './AutoCompleteTextfield';
import Autocomplete from "./autocompletetextfieldcode";
import Data from "../../Data/AutocompleteData";

// test('renders 1st Typography text', () => {
//   render(<AutoCompleteTextfield />);
//   const text = screen.getByTestId("typo1");
//   expect(text).toHaveTextContent("Autocomplete Textfield");
// });

// test('renders image', () => {
//   render(<AutoCompleteTextfield />);
//   expect(screen.getByRole("img")).toBeInTheDocument();
// });

// test('renders package', () => {
//   render(<Autocomplete data={Data.Countries} />);
//   expect(screen.getByRole("combobox")).toBeInTheDocument();
// });

// test('renders package', () => {
//   render(<Autocomplete data={Data.Countries} />);
//   expect(screen.getByRole("button")).toBeInTheDocument();
// });

describe("Autocomplete", () => {
  it("Renders the Autocomplete headings", () => {
    render(<AutoCompleteTextfield />);
    screen.getByRole("heading", { level: 5, name: "Autocomplete Textfield" }); 
    screen.getByRole("heading", { level: 6, name: "Installation" });
    screen.getByRole("heading", { level: 6, name: "Layout" });
    screen.getByRole("heading", { level: 6, name: "Default" });
    screen.getByRole("heading", { level: 6, name: "Usage" });
    screen.getByRole("heading", { level: 6, name: "App.js" });
  })
  it("Renders the Autocomplete text", () => {
    render(<AutoCompleteTextfield />);
    screen.getByText(
      "The AutoComplete component provides the matched suggestion list when type into the input, from which the user can select one."
    );
    screen.getByText(
      'The widget is useful for setting the value of a single-line textbox in one of two types of scenarios: The value for the textbox must be chosen from a predefined set of allowed values, e.g., a location field must contain a valid location name: combo box. The textbox may contain any arbitrary value, but it is advantageous to suggest possible values to the user, e.g., a search field may suggest similar or previous searches to save the user time: free solo. It\'s meant to be an improved version of the "react-select" and "downshift" packages.'
    );
  });
  it("Renders the Autocomplete image", () => {
    render(<AutoCompleteTextfield />);
    screen.getByTestId("autocomplete-image");
  });
  it("Renders Autocomplete without data", () => {
    render(
      <Autocomplete />
    );
    expect(screen.getByLabelText("Choose a country")).toBeInTheDocument();
    // expect(screen.getByRole("combobox")).toBeInTheDocument();
    expect(screen.getByRole("button")).toBeInTheDocument();
  });
  it("Renders Autocomplete with data", () => {
    render(
      <Autocomplete data={Data.Countries} />
    );
    const autocomplete = screen.getByTestId('autocomplete');
    const input = within(autocomplete).getByRole('combobox')
    autocomplete.focus()

   // assign value to input field
   fireEvent.change(input, { target: { value: "Uni" } })
  //  await
   // navigate to the first item in the autocomplete box
   fireEvent.keyDown(autocomplete, { key: 'ArrowDown' })
  //  await
   // select the first item
   fireEvent.keyDown(autocomplete, { key: 'Enter' })
  //  await
   // check the new value of the input field
   expect(input).toHaveValue('United Arab Emirates')
  });
})