import * as React from "react";
import Typography from "@mui/material/Typography";
import DevelopmentIcon from "../Assets/soft-development.png";
import CalenderIcon from "../Assets/calender.png";
import MainMenuIcon from "../Assets/main-menu.png";
import RadioButtonIcon from "../Assets/radio-button.png";
import Grid from "@mui/material/Grid";
import { Link } from "react-router-dom";
import CodePen from "../Icons/codepen.png";

export default function Dashboard() {
  return (
    <div>
      <Typography
        variant="h6"
        component="div"
        role="heading"
        aria-level={6}
        className="topHeading"
      >
        Move faster with intuitive React UI tools
      </Typography>
      <Grid container rowSpacing={4} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
        <Grid item xs={12} md={12} sm={12} lg={8}>
          <Typography
            component="p"
            className="topHeadingSub"
            style={{ marginBottom: 20 }}
          >
            Pwc React Library offers a comprehensive suite of UI tools to help
            you ship new features faster. Start with Pwc React Library, our
            fully-loaded component library, or bring your own design system to
            our production-ready components.
            <br /> <br />
            Pwc React Library is a library of React UI components and an
            open-source React component library.
          </Typography>
          <Typography
            component="p"
            className="topHeadingSub"
            style={{ marginBottom: 20 }}
          >
            It includes a comprehensive collection of prebuilt components that
            are ready for use in production right out of the box. Pwc React
            Library is beautiful by design and features a suite of customization
            options that make it easy to implement your own custom design system
            on top of our components.
          </Typography>
          <Typography
            variant="h6"
            component="div"
            style={{
              marginLeft: 10,
              marginTop: 54,
              display: "flex",
              position: "relative",
            }}
            role="heading"
            aria-level={6}
            className="titleHeading"
          >
            <img
              src={CodePen}
              alt="codePen"
              width="40px"
              height="40px"
              style={{ marginRight: "14px" }}
            />{" "}
            Installation
          </Typography>
          <p
            style={{
              marginTop: 18,
              marginBottom: 37,
              background: "#F4F4F4",
              height: 72,
              padding: 20,
              borderRadius: 11,
              boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
              fontFamily: "Helvetica Neue LT Pro",
              fontStyle: "normal",
              fontWeight: 500,
              fontSize: 18,
              lineHeight: 2,
              color: "#636161",
              paddingLeft: 38,
            }}
          >
            npm i pwc-react-library
          </p>
        </Grid>
        <Grid item xs={12} md={12} sm={12} lg={4}>
          <div
            style={{
              borderRadius: 42,
              maxWidth: 255,
              padding: "10px 18px",
              textAlign: "center",
              backgroundColor: "#D93954",
            }}
          >
            <p className="titleHeading info">Pwc React Library</p>
            <img
              src={DevelopmentIcon}
              style={{ marginBottom: "15px" }}
              alt="codePen"
            />
          </div>
        </Grid>
      </Grid>
      <Typography
        variant="h5"
        component="h5"
        role="heading"
        aria-level={5}
        className="topHeading info"
      >
        Most Used Widget Panel
      </Typography>
      <Grid container spacing={{ xs: 2, md: 3 }}>
        <Grid item xs={12} sm={12} md={4} key={1}>
          <Link style={{ textDecoration: "none" }} to="/date">
            <div className="mainDivInfo">
              <p className="topHeadingSub info">Datepicker</p>
              <Typography component="div" className="widgetSubHeading">
                Simple React datepicker component calendars with the ability to
                select the date in single, multiple and range modes.
              </Typography>
              <div className="mainDivImg">
                <img src={CalenderIcon} alt="calendericon" />
              </div>
            </div>
          </Link>
        </Grid>
        <Grid item xs={12} sm={12} md={4} key={2}>
          <Link style={{ textDecoration: "none" }} to="/dropdown">
            <div className="mainDivInfo">
              <p className="topHeadingSub info">Dropdown</p>
              <Typography component="div" className="widgetSubHeading">
                A React component which provides multi select functionality with
                various features like selection limit, CSS customization....
              </Typography>
              <div className="mainDivImg">
                <img src={MainMenuIcon} alt="mainmenuicon" />
              </div>
            </div>
          </Link>
        </Grid>
        <Grid item xs={12} sm={12} md={4} key={3}>
          <Link style={{ textDecoration: "none" }} to="/accordion">
            <div className="mainDivInfo">
              <p className="topHeadingSub info">Accordion</p>
              <Typography component="div" className="widgetSubHeading">
                Material UI is a comprehensive library of components that
                features our implementation of Google's Material Design system.
              </Typography>

              <div className="mainDivImg">
                <img src={RadioButtonIcon} alt="radiobuttonicon" />
              </div>
            </div>
          </Link>
        </Grid>
      </Grid>
    </div>
  );
}
