import React, { useState } from "react";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemText from "@mui/material/ListItemText";
import { Link } from "react-router-dom";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import ExpandMoreSharpIcon from "@mui/icons-material/ExpandMoreSharp";
import List from "@mui/material/List";

const MainListItems = () => {
  const [btnClass, setBtnClass] = useState(window.location.pathname);
  const [openInputs, setOpenInputs] = useState(false);
  const [openDataDisplay, setDataDisplay] = useState(false);
  const [openFeedback, setFeedback] = useState(false);
  const [openSurfaces, setSurfaces] = useState(false);
  const [openLayout, setLayout] = useState(false);

  function handleInputs() {
    setOpenInputs((openInputs) => !openInputs);
    addColor("Inputs");
  }

  function handleDataDisplay() {
    setDataDisplay((openDataDisplay) => !openDataDisplay);
    addColor("DataDisplay");
  }

  function handleFeedback() {
    setFeedback((openFeedback) => !openFeedback);
    addColor("Feedback");
  }

  function handleSurfaces() {
    setSurfaces((openSurfaces) => !openSurfaces);
    addColor("Surfaces");
  }

  function handleLayout() {
    setLayout((openLayout) => !openLayout);
    addColor("Layout");
  }

  console.log(window.location.pathname);
  const addColor = (compName) => {
    if (compName === "Dashboard") {
      setBtnClass("/");
    } else if (compName === "Datepicker") {
      setBtnClass("/date");
    } else if (compName === "Dropdown") {
      setBtnClass("/dropdown");
    } else if (compName === "Accordion") {
      setBtnClass("/accordion");
    } else if (compName === "Chart") {
      setBtnClass("/chart");
    } else if (compName === "Autocomplete") {
      setBtnClass("/autocompletetextfield");
    } else if (compName === "Button") {
      setBtnClass("/button");
    } else if (compName === "Alert") {
      setBtnClass("/alert");
    } else if (compName === "Typography") {
      setBtnClass("/typography");
    } else if (compName === "Rating") {
      setBtnClass("/rating");
    } else if (compName === "Switch") {
      setBtnClass("/switch");
    } else if (compName === "Checkbox") {
      setBtnClass("/checkbox");
    } else if (compName === "TextField") {
      setBtnClass("/textfield");
    } else if (compName === "Inputs") {
      setBtnClass("/inputs");
    } else if (compName === "DataDisplay") {
      setBtnClass("/datadisplay");
    } else if (compName === "Feedback") {
      setBtnClass("/feedback");
    } else if (compName === "Surfaces") {
      setBtnClass("/surfaces");
    } else if (compName === "Layout") {
      setBtnClass("/layout");
    } else if (compName === "Box") {
      setBtnClass("/box");
    } else if (compName === "Grid") {
      setBtnClass("/grid");
    }
  };
  return (
    <React.Fragment>
      <Link to="/" style={{ textDecoration: "none", color: "#ffff" }}>
        <ListItemButton
          onClick={() => addColor("Dashboard")}
          className={btnClass === "/" ? "addWhite" : ""}
        >
          <ListItemText primary="DASHBOARD" />
        </ListItemButton>
      </Link>
      <ListItemButton
        style={{ textDecoration: "none", color: "#ffff" }}
        onClick={handleInputs}
        className={btnClass === "/inputs" ? "addWhite" : ""}
      >
        <ListItemText primary="INPUTS" />
        {openInputs ? (
          <ExpandMoreSharpIcon
            sx={{ color: "white" }}
            style={{ marginLeft: "-103px", marginTop: "2px" }}
          />
        ) : (
          <ChevronRightIcon
            sx={{ color: "white" }}
            style={{ marginLeft: "-103px", marginTop: "2px" }}
          />
        )}
      </ListItemButton>
      {openInputs && (
        <List sx={{ marginLeft: "10px" }}>
          <Link
            to="/button"
            style={{
              textDecoration: "none",
              color: "#ffff",
            }}
          >
            <ListItemButton
              onClick={() => addColor("Button")}
              className={btnClass === "/button" ? "addWhite" : ""}
            >
              <ListItemText primary="Button" />
            </ListItemButton>
          </Link>
          <Link
            to="/checkbox"
            style={{
              textDecoration: "none",
              color: "#ffff",
            }}
          >
            <ListItemButton
              onClick={() => addColor("Checkbox")}
              className={btnClass === "/checkbox" ? "addWhite" : ""}
            >
              <ListItemText primary="Checkbox" />
            </ListItemButton>
          </Link>
          <Link
            to="/rating"
            style={{
              textDecoration: "none",
              color: "#ffff",
            }}
          >
            <ListItemButton
              onClick={() => addColor("Rating")}
              className={btnClass === "/rating" ? "addWhite" : ""}
            >
              <ListItemText primary="Rating" />
            </ListItemButton>
          </Link>
          <Link
            to="/switch"
            style={{
              textDecoration: "none",
              color: "#ffff",
            }}
          >
            <ListItemButton
              onClick={() => addColor("Switch")}
              className={btnClass === "/switch" ? "addWhite" : ""}
            >
              <ListItemText primary="Switch" />
            </ListItemButton>
          </Link>
          <Link
            to="/textfield"
            style={{
              textDecoration: "none",
              color: "#ffff",
            }}
          >
            <ListItemButton
              onClick={() => addColor("TextField")}
              className={btnClass === "/textfield" ? "addWhite" : ""}
            >
              <ListItemText primary="Text Field" />
            </ListItemButton>
          </Link>
        </List>
      )}
      <ListItemButton
        style={{ textDecoration: "none", color: "#ffff" }}
        onClick={handleDataDisplay}
        className={btnClass === "/datadisplay" ? "addWhite" : ""}
      >
        <ListItemText primary="DATA DISPLAY" />
        {openDataDisplay ? (
          <ExpandMoreSharpIcon
            sx={{ color: "white" }}
            style={{ marginLeft: "-50px", marginTop: "2px" }}
          />
        ) : (
          <ChevronRightIcon
            sx={{ color: "white" }}
            style={{ marginLeft: "-50px", marginTop: "2px" }}
          />
        )}
      </ListItemButton>
      {openDataDisplay && (
        <List sx={{ marginLeft: "10px" }}>
          <Link
            to="/typography"
            style={{
              textDecoration: "none",
              color: "#ffff",
            }}
          >
            <ListItemButton
              onClick={() => addColor("Typography")}
              className={btnClass === "/typography" ? "addWhite" : ""}
            >
              <ListItemText primary="Typography" />
            </ListItemButton>
          </Link>
        </List>
      )}

      <ListItemButton
        style={{ textDecoration: "none", color: "#ffff" }}
        onClick={handleFeedback}
        className={btnClass === "/feedback" ? "addWhite" : ""}
      >
        <ListItemText primary="FEEDBACK" />
        {openFeedback ? (
          <ExpandMoreSharpIcon
            sx={{ color: "white" }}
            style={{ marginLeft: "-75px", marginTop: "2px" }}
          />
        ) : (
          <ChevronRightIcon
            sx={{ color: "white" }}
            style={{ marginLeft: "-75px", marginTop: "2px" }}
          />
        )}
      </ListItemButton>
      {openFeedback && (
        <List sx={{ marginLeft: "10px" }}>
          <Link
            to="/alert"
            style={{
              textDecoration: "none",
              color: "#ffff",
            }}
          >
            <ListItemButton
              onClick={() => addColor("Alert")}
              className={btnClass === "/alert" ? "addWhite" : ""}
            >
              <ListItemText primary="Alert" />
            </ListItemButton>
          </Link>
        </List>
      )}

      <ListItemButton
        style={{ textDecoration: "none", color: "#ffff" }}
        onClick={handleSurfaces}
        className={btnClass === "/surfaces" ? "addWhite" : ""}
      >
        <ListItemText primary="SURFACES" />
        {openSurfaces ? (
          <ExpandMoreSharpIcon
            sx={{ color: "white" }}
            style={{ marginLeft: "-75px", marginTop: "2px" }}
          />
        ) : (
          <ChevronRightIcon
            sx={{ color: "white" }}
            style={{ marginLeft: "-75px", marginTop: "2px" }}
          />
        )}
      </ListItemButton>
      {openSurfaces && (
        <List sx={{ marginLeft: "10px" }}>
          <Link
            to="/accordion"
            style={{ textDecoration: "none", color: "#ffff" }}
          >
            <ListItemButton
              onClick={() => addColor("Accordion")}
              className={btnClass === "/accordion" ? "addWhite" : ""}
            >
              <ListItemText primary="Accordion" />
            </ListItemButton>
          </Link>
          <Link
            to="/autocompletetextfield"
            style={{
              textDecoration: "none",
              color: "#2D2D2D",
              whiteSpace: "break-spaces",
            }}
          >
            <ListItemButton
              onClick={() => addColor("Autocomplete")}
              className={
                btnClass === "/autocompletetextfield"
                  ? "addWhiteAllign"
                  : "removePadding"
              }
            >
              <ListItemText primary="Autocomplete Textfield" />
            </ListItemButton>
          </Link>
          <Link
            to="/chart"
            style={{ textDecoration: "none", color: "#2D2D2D" }}
          >
            <ListItemButton
              onClick={() => addColor("Chart")}
              className={btnClass === "/chart" ? "addWhite" : ""}
            >
              <ListItemText primary="Chart And Graph" />
            </ListItemButton>
          </Link>
          <Link to="/date" style={{ textDecoration: "none", color: "#ffff" }}>
            <ListItemButton
              onClick={() => addColor("Datepicker")}
              className={btnClass === "/date" ? "addWhite" : ""}
            >
              <ListItemText primary="Datepicker" />
            </ListItemButton>
          </Link>
          <Link
            to="/dropdown"
            style={{ textDecoration: "none", color: "#ffff" }}
          >
            <ListItemButton
              onClick={() => addColor("Dropdown")}
              className={btnClass === "/dropdown" ? "addWhite" : ""}
            >
              <ListItemText primary="Dropdown" />
            </ListItemButton>
          </Link>
        </List>
      )}
      <ListItemButton
        style={{ textDecoration: "none", color: "#ffff" }}
        onClick={handleLayout}
        className={btnClass === "/layout" ? "addWhite" : ""}
      >
        <ListItemText primary="LAYOUT" />
        {openLayout ? (
          <ExpandMoreSharpIcon
            sx={{ color: "white" }}
            style={{ marginLeft: "-103px", marginTop: "2px" }}
          />
        ) : (
          <ChevronRightIcon
            sx={{ color: "white" }}
            style={{ marginLeft: "-103px", marginTop: "2px" }}
          />
        )}
      </ListItemButton>
      {openLayout && (
        <List sx={{ marginLeft: "10px" }}>
          <Link to="/grid" style={{ textDecoration: "none", color: "#ffff" }}>
            <ListItemButton
              onClick={() => addColor("Grid")}
              className={btnClass === "/grid" ? "addWhite" : ""}
            >
              <ListItemText primary="Grid" />
            </ListItemButton>
          </Link>
          <Link to="/box" style={{ textDecoration: "none", color: "#ffff" }}>
            <ListItemButton
              onClick={() => addColor("Box")}
              className={btnClass === "/box" ? "addWhite" : ""}
            >
              <ListItemText primary="Box" />
            </ListItemButton>
          </Link>
        </List>
      )}
    </React.Fragment>
  );
};
export default MainListItems;
