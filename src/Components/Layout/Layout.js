import React from 'react';
import styled from "styled-components";

const isMobile = () => {
    return
  // some js way to detect if user is on a mobile device
}

const MobileLayout = styled.div`

`;

const DesktopLayout = styled.div`

`;

const Layout = (props) => {
  return (
    <MobileLayout>
      {props.children}
    </MobileLayout>
  )
}

export default Layout
