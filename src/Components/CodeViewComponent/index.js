import React from "react";
import { Typography } from "@mui/material/";
import CPU from "../Icons/cpu.png";

import styled from "styled-components";

const IconElement = styled.img`
  margin-right: 14px;
  width: 40px;
  height: 40px;
`;
const descriptionStyle = { marginBottom: 55, marginLeft: 67 };
const titleStyle = { marginLeft: 10, display: "flex", position: "relative" };

export const CodeViewComponent = ({ title, description, ...props }) => {
  return (
    <div>
      <Typography variant="h6" component="div" style={titleStyle} role="heading" aria-level={6} className="titleHeading">
        <IconElement src={CPU} alt="Award" />
        {title}
      </Typography>
      <Typography component="p" style={descriptionStyle} className="topHeadingSub">
        {description}
      </Typography>
      {props.children}
    </div>
  );
};
