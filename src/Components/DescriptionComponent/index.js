import React from 'react';
import Typography from "@mui/material/Typography";

export const DescriptionComponent = ({ title, description }) => {
  return (
    <>
        <Typography
          variant="h5"
          component="h5"
          role="heading"
          aria-level={5}
          className="topHeading"
        >
         { title }
        </Typography>
        <Typography component="p" className="topHeadingSub">
          { description }
        </Typography>
    </>
  );
};

