import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import { CardContent } from "@mui/material";
import Card from "@mui/material/Card";
import Graph1 from "../Assets/Graph1.png";
import Graph2 from "../Assets/Graph2.png";
import Graph3 from "../Assets/Graph3.png";
import Graph4 from "../Assets/Graph4.png";
import { Graph } from "graphandchart";
import Cards from "../Cards/Cards";
import Data from "../../Data/ChartData";
import CodePen from "../Icons/codepen.png";
import Command from "../Icons/command.png";
import Award from "../Icons/award.png";
import CPU from "../Icons/cpu.png";

const GraphWhite1 = "Graph-White";
const GraphBlack1 = "Graph-Black";

export default function ChartGraph() {
  return (
    <div>
      <Typography variant="h5" component="h5" className="topHeading">
        Chart And Graph
      </Typography>
      <Typography component="p" className="topHeadingSub">
        React Chart Component supports Area, Spline Area, Stacked Area, Stacked
        Area 100, Step Area, Range Area and Range Spline Area Charts.
      </Typography>
      <Typography
        variant="h6"
        component="div"
        style={{
          marginLeft: 10,
          marginTop: 54,
          display: "flex",
          position: "relative",
        }}
        role="heading"
        aria-level={6}
        className="titleHeading"
      >
        <img
          src={CodePen}
          alt="codePen"
          width="40px"
          height="40px"
          style={{ marginRight: "14px" }}
        />{" "}
        Installation
      </Typography>
      <p
        style={{
          marginTop: 18,
          marginBottom: 37,
          background: "#F4F4F4",
          height: 72,
          padding: 20,
          borderRadius: 11,
          boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
          fontFamily: "Helvetica Neue LT Pro",
          fontStyle: "normal",
          fontWeight: 500,
          fontSize: 18,
          lineHeight: 2,
          color: "#636161",
          paddingLeft: 38,
        }}
      >
        npm i graphandchart
      </p>
      <Typography
        variant="h6"
        component="div"
        style={{ marginLeft: 10, display: "flex", position: "relative" }}
        role="heading"
        aria-level={6}
        className="titleHeading"
      >
        <img
          src={Command}
          alt="Command"
          width="40px"
          height="40px"
          style={{ marginRight: "14px" }}
        />
        Layout
      </Typography>
      <Typography
        component="p"
        className="topHeadingSub"
        style={{ marginBottom: 50, marginTop: 18, marginLeft: 55 }}
      >
        React Charts {"&"} Graphs Component with 10x Performance for Web
        Applications. Charts are interactive, responsive and support animation,
        zooming, panning, events, exporting chart as image, drilldown {"&"}
        real-time updates. React Chart library comes with 30+ chart types
        including line, column, bar, pie, doughnut, range charts, stacked
        charts, stock charts, etc. With these high performing charts, you can
        add hundreds of thousands of data points without causing performance
        lag.
      </Typography>
      <Box
        style={{
          marginBottom: 75,
          marginLeft: 55,
          backgroundColor: "#F3F3F3",
          paddingBottom: 12,
          paddingTop: 12,
          paddingLeft: 12,
        }}
      >
        <Grid container>
          <Grid item xs={3}>
            <img
              src={Graph1}
              alt="Graph1"
              height="200"
              width="200"
              data-testid={GraphWhite1}
            />
          </Grid>
          <Grid item xs={3}>
            <img
              src={Graph2}
              alt="Graph2"
              height="200"
              width="200"
              data-testid={GraphBlack1}
            />
          </Grid>
          <Grid item xs={3}>
            <img src={Graph3} alt="Graph3" height="200" width="200" />
          </Grid>
          <Grid item xs={3}>
            <img src={Graph4} alt="Graph4" height="200" width="200" />
          </Grid>
        </Grid>
      </Box>

      <Box>
        <Grid container spacing={4}>
          <Grid item xs={6}>
            <Typography
              variant="h6"
              component="div"
              style={{ marginLeft: 10, display: "flex", position: "relative" }}
              role="heading"
              aria-level={6}
              className="titleHeading"
            >
              <img
                src={Award}
                alt="Award"
                width="40px"
                height="40px"
                style={{ marginRight: "14px" }}
              />
              Default View
            </Typography>
            <Typography
              component="p"
              style={{ marginBottom: 55, marginLeft: 67 }}
              className="topHeadingSub"
            >
              React Chart component supports rendering multiple axes with
              different scales on either sides. This is useful while rendering
              multi-series chart with different dimensions / range of values.
              Below example shows line chart with multiple axes alongside source
              code that you can run locally.
            </Typography>
            <Card
              style={{
                background: "#F4F4F4",
                height: 301,
                width: 445,
                borderRadius: 11,
                boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
              }}
            >
              <CardContent>
              <div
                style={{
                  fontFamily: "Helvetica Neue LT Pro",
                  fontStyle: "normal",
                  fontWeight: 500,
                  fontSize: 16,
                  lineHeight: 3,
                  color: "#636161",
                  paddingTop: 10,
                  paddingLeft: 32,
                  height: "200px"
                }}
              >
                Chart And Graph
                <Graph options={Data.ChartOptions} />
              </div>
            </CardContent>
            </Card>
          </Grid>
          <Grid item xs={6}>
            <Typography
              variant="h6"
              component="div"
              style={{ marginLeft: 10, display: "flex", position: "relative" }}
              role="heading"
              aria-level={6}
              className="titleHeading"
            >
              <img
                src={CPU}
                alt="Award"
                width="40px"
                height="40px"
                style={{ marginRight: "14px" }}
              />
              Usage (Code View)
            </Typography>
            <Typography
              component="p"
              style={{ marginBottom: 180, marginLeft: 67 }}
              className="topHeadingSub"
            >
              Usage
            </Typography>
            <p
              style={{
                background: "#2D2D2D",
                width: 445,
                height: 301,
                borderRadius: 11,
                boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
                overflowY: 'scroll'
              }}
            >
              <div
                style={{
                  fontFamily: "Helvetica Neue LT Pro",
                  fontStyle: "normal",
                  fontWeight: 500,
                  fontSize: 16,
                  color: "#FFFFFF",
                  padding: "26px 24px 16px 50px",
                  whiteSpace: "pre",
                }}
              >
                {Data.ChartCode}
              </div>
            </p>
          </Grid>
        </Grid>
      </Box>
    </div>
  );
}
