import React from "react";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";
import CodePen from "../Icons/codepen.png";
import Award from "../Icons/award.png";
import { Typography as PwCTypography } from "pwc-react-library";

const typographyImport = `import { Typography } from "pwc-react-library";`;
const typographyH1Code = `<Typography component="h1" content="h1. Heading" variant="h1">h1. Heading</Typhography>`;
const typographyH2Code = `<Typography component="h2" content="h2. Heading" variant="h2">h2. Heading</Typhography>`;
const typographyH3Code = `<Typography component="h3" content="h3. Heading" variant="h3">h3. Heading</Typhography>`;
const typographyH4Code = `<Typography component="h4" content="h4. Heading" variant="h4">h4. Heading</Typhography>`;
const typographyH5Code = `<Typography component="h5" content="h5. Heading" variant="h5">h5. Heading</Typhography>`;
const typographyH6Code = `<Typography component="h6" content="h6. Heading" variant="h6">h6. Heading</Typhography>`;
const typographySubtitle1Code = `<Typography component="h6" content="subtitle1. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos blanditiis tenetur" variant="subtitle1" >subtitle1. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos blanditiis tenetur</Typhography>`;
const typographySubtitle2Code = `<Typography component="h6" content="subtitle2. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos blanditiis tenetur" variant="subtitle2" >subtitle2. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos blanditiis tenetur</Typhography>`;
const typographyCaption = `<Typography content="Caption text" variant="caption" >Caption text</Typhography>`;

const TypographyComponent = () => {
  const [showBasicButtons, setShowBasicButtons] = React.useState(false);

  return (
    <div>
      <Typography variant="h5" component="h5" role="heading" aria-level={5} className="topHeading">
        Typography
      </Typography>
      <Typography component="p" className="topHeadingSub">
        Typography allow users to show the text, caption data in the application.
      </Typography>
      <Typography
        variant="h6"
        component="div"
        style={{
          marginTop: 30,
          display: "flex",
          position: "relative",
        }}
        role="heading"
        aria-level={6}
        className="titleHeading"
        gutterBottom
      >
        <img src={CodePen} alt="codePen" width="40px" height="40px" style={{ marginRight: "14px" }} /> Import
      </Typography>
      <p
        style={{
          background: "#2D2D2D",
          height: "auto",
          width: "auto",
          borderRadius: 11,
          boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
        }}
      >
        <div
          style={{
            fontFamily: "Helvetica Neue LT Pro",
            fontStyle: "normal",
            fontWeight: 500,
            fontSize: 16,
            color: "#E1C16E",
            padding: "20px",
            whiteSpace: "break-spaces",
          }}
        >
          {typographyImport}
        </div>
      </p>
      <Typography variant="h6" component="div" style={{ display: "flex", position: "relative" }} role="heading" aria-level={6} className="titleHeading">
        <img src={Award} alt="Award" width="40px" height="40px" style={{ marginRight: "14px" }} />
        Typography
      </Typography>
      <Typography component="p" style={{ marginBottom: 30, marginLeft: 57 }} className="topHeadingSub">
        The Typography comes with different size like: h1, h2, h3, h5, h6, caption etc..
      </Typography>
      <p
        style={{
          height: "auto",
          width: "auto",
          borderRadius: 6.5,
          border: "2px solid grey",
          alignItems: "center",
          display: "flex",
          justifyContent: "center",
          position: "relative",
        }}
      >
        <div
          style={{
            fontFamily: "Helvetica Neue LT Pro",
            fontStyle: "normal",
            fontWeight: 500,
            fontSize: 16,
            padding: "20px",
          }}
        >
          <Grid container spacing={2}>
            <Grid item>
              <PwCTypography component="h1" content="h1. Heading" style={{}} variant="h1">
                h1. Heading
              </PwCTypography>
              <PwCTypography component="h2" content="h2. Heading" style={{}} variant="h2">
                h2. Heading
              </PwCTypography>
              <PwCTypography component="h3" content="h3. Heading" style={{}} variant="h3">
                h3. Heading
              </PwCTypography>
              <PwCTypography component="h4" content="h4. Heading" style={{}} variant="h4">
                h4. Heading
              </PwCTypography>
              <PwCTypography component="h5" content="h5. Heading" style={{}} variant="h5">
                h5. Heading
              </PwCTypography>
              <PwCTypography component="h6" content="h6. Heading" style={{}} variant="h6">
                h6. Heading
              </PwCTypography>
              <PwCTypography component="h6" content="subtitle1. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos blanditiis tenetur" style={{}} variant="subtitle1">
                subtitle1. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos blanditiis tenetur
              </PwCTypography>
              <PwCTypography component="h6" content="subtitle2. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos blanditiis tenetur" style={{}} variant="subtitle2">
                subtitle2. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos blanditiis tenetur
              </PwCTypography>
              <PwCTypography content="Caption text" style={{}} variant="caption">
                Caption text
              </PwCTypography>
            </Grid>
            <Grid item>
              <div className="show">
                <button onClick={() => setShowBasicButtons(!showBasicButtons)} className="btn">
                  {showBasicButtons ? "hide code" : "show code"}
                </button>
              </div>
            </Grid>
          </Grid>
        </div>
      </p>
      {showBasicButtons && (
        <div>
          <p
            style={{
              background: "#2D2D2D",
              height: "auto",
              width: "auto",
              borderRadius: 11,
              boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
            }}
          >
            <div
              style={{
                fontFamily: "Helvetica Neue LT Pro",
                fontStyle: "normal",
                fontWeight: 500,
                fontSize: 16,
                color: "#E1C16E",
                padding: "20px",
                whiteSpace: "break-spaces",
              }}
            >
              {typographyH1Code}
              <br />
              {typographyH2Code}
              <br />
              {typographyH3Code}
              <br />
              {typographyH4Code}
              <br />
              {typographyH5Code}
              <br />
              {typographyH6Code}
              <br />
              {typographySubtitle1Code}
              <br />
              {typographySubtitle2Code}
              <br />
              {typographyCaption}
            </div>
          </p>
        </div>
      )}
      <br />
    </div>
  );
};
export default TypographyComponent;
