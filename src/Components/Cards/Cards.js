import * as React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";

export default function BasicCard(props) {
  return (
    <Card sx={{ minWidth: 10 }}>
      <CardContent style={{ background: "#F4F4F4" }}>
        <Typography variant="h6" component="div">
          {props.head}
        </Typography>
        <Typography component="div">{props.container}</Typography>
      </CardContent>
    </Card>
  );
}
