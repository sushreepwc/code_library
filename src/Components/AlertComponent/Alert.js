import React from "react";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";
import CodePen from "../Icons/codepen.png";
import Award from "../Icons/award.png";
import { Alert, AlertTitle } from "pwc-react-library";

const alertImport = `import { Alert, AlertTitle } from "pwc-react-library";`;
const errorAlertCode = `<Alert closeAble content="This is an error alert — check it out!" onClose={() => {}} type="error">This is an error alert — check it out!</Alert>`;
const warningAlertCode = `<Alert closeAble content="This is an warning alert — check it out!" onClose={() => {}} type="warning">This is an warning alert — check it out!</Alert>`;
const infoAlertCode = `<Alert closeAble content="This is an info alert — check it out!" onClose={() => {}} type="info">This is an info alert — check it out!</Alert>`;
const successAlertCode = ` <Alert closeAble content="This is an success alert — check it out!" onClose={() => {}} type="success">This is an success alert — check it out!</Alert>`;
const errorTitleAlertCode = ` <Alert content="This is an error alert — check it out!" title="Error" type="error"><AlertTitle>Error</AlertTitle>This is an error alert — check it out!</Alert>`
const warningTitleAlertCode =  `<Alert content="This is an warning alert — check it out!" title="Warning" type="warning"><AlertTitle>Warning</AlertTitle>This is an warning alert — check it out!</Alert>`;
const infoTitleAlertCode = `<Alert content="This is an info alert — check it out!" title="Info" type="info"><AlertTitle>Info</AlertTitle>This is an info alert — check it out!</Alert>`;
const successTitleAlertCode = `<Alert content="This is an success alert — check it out!" title="Success" type="success"><AlertTitle>Success</AlertTitle>This is an success alert — check it out!</Alert>`

const AlertComponent = () => {
  const [showBasicButtons, setShowBasicButtons] = React.useState(false);
  const [showTextButtons, setShowTextButtons] = React.useState(false);

  return (
    <div>
      <Typography variant="h5" component="h5" role="heading" aria-level={5} className="topHeading">
        Alert
      </Typography>
      <Typography component="p" className="topHeadingSub">
        Alert allow users to show the success, error, warning message in the application.
      </Typography>
      <Typography
        variant="h6"
        component="div"
        style={{
          marginTop: 30,
          display: "flex",
          position: "relative",
        }}
        role="heading"
        aria-level={6}
        className="titleHeading"
        gutterBottom
      >
        <img src={CodePen} alt="codePen" width="40px" height="40px" style={{ marginRight: "14px" }} /> Import
      </Typography>
      <p
        style={{
          background: "#2D2D2D",
          height: "auto",
          width: "auto",
          borderRadius: 11,
          boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
        }}
      >
        <div
          style={{
            fontFamily: "Helvetica Neue LT Pro",
            fontStyle: "normal",
            fontWeight: 500,
            fontSize: 16,
            color: "#E1C16E",
            padding: "20px",
            whiteSpace: "break-spaces",
          }}
        >
          {alertImport}
        </div>
      </p>
      <Typography variant="h6" component="div" style={{ display: "flex", position: "relative" }} role="heading" aria-level={6} className="titleHeading">
        <img src={Award} alt="Award" width="40px" height="40px" style={{ marginRight: "14px" }} />
        Basic Alert
      </Typography>
      <Typography component="p" style={{ marginBottom: 30, marginLeft: 57 }} className="topHeadingSub">
        The Alert comes with four variants: error, success, info, warning.
      </Typography>
      <p
        style={{
          height: "auto",
          // width: "auto",
          borderRadius: 9,
          border: "2px solid grey",
          alignItems: "center",
          display: "flex",
          justifyContent: "center",
          position: "relative",
        }}
      >
        <div
          style={{
            fontFamily: "Helvetica Neue LT Pro",
            fontStyle: "normal",
            fontWeight: 500,
            fontSize: 16,
            padding: "20px",
            width:"100%",
          }}
        >
          <Grid container spacing={12}>
            <Grid item md={12}>
              <Alert closeAble onClose={() => {}} type="error">
                This is an error alert — check it out!
              </Alert>
              <br />
              <Alert closeAble onClose={() => {}} type="warning">
                This is an warning alert — check it out!
              </Alert>
              <br />
              <Alert closeAble onClose={() => {}} type="info">
                This is an info alert — check it out!
              </Alert>
              <br />
              <Alert closeAble onClose={() => {}} type="success">
                This is an success alert — check it out!
              </Alert>
            </Grid>
            <Grid item>
              <div className="show">
                <button onClick={() => setShowBasicButtons(!showBasicButtons)} className="btn">
                  {showBasicButtons ? "hide code" : "show code"}
                </button>
              </div>
            </Grid>
          </Grid>
        </div>
      </p>
      {showBasicButtons && (
        <div>
          <p
            style={{
              background: "#2D2D2D",
              height: "auto",
              width: "auto",
              borderRadius: 11,
              boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
            }}
          >
            <div
              style={{
                fontFamily: "Helvetica Neue LT Pro",
                fontStyle: "normal",
                fontWeight: 500,
                fontSize: 16,
                color: "#E1C16E",
                padding: "20px 20px 0px 20px",
                whiteSpace: "break-spaces",
              }}
            >
              {errorAlertCode}
              <br />
              {warningAlertCode}
              <br />
              {infoAlertCode}
              <br />
              {successAlertCode}
            </div>
          </p>
        </div>
      )}
      <br />
      <Typography variant="h6" component="div" style={{ display: "flex", position: "relative" }} role="heading" aria-level={6} className="titleHeading" gutterBottom>
        Alert With Title
      </Typography>
      <Typography component="p" style={{ marginBottom: 30 }} className="topHeadingSub" gutterBottom>
        Alert with title alert used for show the title in alert.
      </Typography>
      <p
        style={{
          height: "auto",
          width: "auto",
          borderRadius: 6.5,
          border: "2px solid grey",
          alignItems: "center",
          display: "flex",
          justifyContent: "center",
          position: "relative",
        }}
      >
        <div
          style={{
            fontFamily: "Helvetica Neue LT Pro",
            fontStyle: "normal",
            fontWeight: 500,
            fontSize: 16,
            padding: "20px 20px 100px 20px",
            width:"100%"
          }}
        >
          <Grid container spacing={12}>
            <Grid item md={12}>
              <Alert title="Error" type="error">
                <AlertTitle>Error</AlertTitle>
                This is an error alert — check it out!
              </Alert>
              <br />
              <Alert title="Warning" type="warning">
                <AlertTitle>Warning</AlertTitle>
                This is an warning alert — check it out!
              </Alert>
              <br />
              <Alert title="Info" type="info">
                <AlertTitle>Info</AlertTitle>
                This is an info alert — check it out!
              </Alert>
              <br />
              <Alert title="Success" type="success">
                <AlertTitle>Success</AlertTitle>
                This is an success alert — check it out!
              </Alert>
            </Grid>
          </Grid>
          <Grid item>
            <div className="show">
              <button onClick={() => setShowTextButtons(!showTextButtons)} className="btn">
                {showTextButtons ? "hide code" : "show code"}
              </button>
            </div>
          </Grid>
        </div>
      </p>
      <br />
      {showTextButtons && (
        <p
          style={{
            background: "#2D2D2D",
            height: "auto",
            width: "auto",
            borderRadius: 11,
            boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
          }}
        >
          <div
            style={{
              fontFamily: "Helvetica Neue LT Pro",
              fontStyle: "normal",
              fontWeight: 500,
              fontSize: 16,
              color: "#E1C16E",
              padding: "20px",
              whiteSpace: "break-spaces",
            }}
          >
            {errorTitleAlertCode}
            <br />
            {warningTitleAlertCode}
            <br/>
            {infoTitleAlertCode}
            <br/>
            {successTitleAlertCode}
          </div>
        </p>
      )}
      <br />
    </div>
  );
};
export default AlertComponent;
