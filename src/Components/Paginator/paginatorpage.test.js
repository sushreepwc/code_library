import React from "react";
import { render, screen } from "@testing-library/react";
import PaginatorPage from "./paginatorpage";

describe("Paginator", () => {
  it("Renders the Paginator headings", () => {
    render(<PaginatorPage />);
    screen.getByRole("heading", { level: 5, name: "Paginator" });
    screen.getByRole("heading", { level: 6, name: "Layout" });
    screen.getByRole("heading", { level: 6, name: "Default" });
    screen.getByRole("heading", { level: 6, name: "Instruction" });
    screen.getByRole("heading", { level: 6, name: "Usage" });
    screen.getByRole("heading", { level: 6, name: "App.js" });
  });

  it("Renders the Paginator text", () => {
    render(<PaginatorPage />);
    screen.getByText(
      "The Paginator component can be used when we are getting 50+ records from an API, but want to show only 10-15 records at a time."
    );
  });

  it("Renders the Paginator images ", () => {
    render(<PaginatorPage />);
    screen.getByTestId("Paginator-Img");
  });
});
