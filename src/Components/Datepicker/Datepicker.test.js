import React from "react";
import { fireEvent, render, screen } from "@testing-library/react";
import Datepicker from "./Datepicker";
import MultiDate1 from "../Assets/Multi2.png";
import MultiDatebydate from "../Assets/Multi3.png";
import MultiDateRange from "../Assets/Range2.png";
import MultiDate1TillRange from "../Assets/Range3.png";
import { Code } from "@material-ui/icons";


describe("Datepicker", () => {
  it("Renders the datepicker headings", () => {
    render(<Datepicker />);
    screen.getByRole("heading", { level: 5, name: "Datepicker" });
    screen.getByRole("heading", { level: 5, name: "Multi Date Picker" });
    screen.getByRole("heading", { level: 5, name: "Date Range Picker" });
    screen.getByRole("heading", { level: 6, name: "Installation" });
    screen.getByRole("heading", { level: 6, name: "Layout" });
    screen.getByRole("heading", { level: 6, name: "Default" });
    screen.getByRole("heading", { level: 6, name: "Instruction" });
    screen.getByRole("heading", { level: 6, name: "Usage" });
    screen.getByRole("heading", { level: 6, name: "App.js" });
    
  });
  it("Renders the datepicker text", () => {
    render(<Datepicker />);
        screen.getByText(
            "Simple React datepicker component calendars with the ability to select the date in single, multiple and range modes."
        );
        screen.getByText(
            "You’ll need to install React and PropTypes separately since those dependencies aren’t included in the package. If you need to use a locale other than the default en-US, you'll also need to import that into your project from date-fns (see Localization section below). Below is a simple example of how to use the Datepicker in a React view. You will also need to require the CSS file from this package (or provide your own). The example below shows how to include the CSS from this package if your build system supports requiring CSS files (Webpack is one that does)."
        );
        screen.getByText(
            "The user can select multiple dates and date range."
        );
        screen.getByText(
            "npm i multiple-date-range-picker"
        );
        screen.getByText(
            "1. Install the library using the above command"
        );
        screen.getByText(
            "2. Import DatepickerCode hook. you can pass props like multiple if you want to select multiple dates otherwise for date range you don't need to pass anything."
        );
        screen.getByText(
            "3. Pass the library the children(<DatepickerCode multiple>)"
        );
        screen.getByText(
            "4. No extra set up needed"
        );
    });
    it('Renders the datepicker images', async () => {
        render(<Datepicker />);
        const multidatepicker = screen.getByAltText('multidatepicker');
        expect(multidatepicker).toHaveAttribute("src",MultiDate1);

        const multidatepickervalue = screen.getByAltText('multidatepickervalue');
        expect(multidatepickervalue).toHaveAttribute("src",MultiDatebydate);

        const daterangepicker = screen.getByAltText('daterangepicker');
        expect(daterangepicker).toHaveAttribute("src",MultiDateRange);

        const daterangepickervalue = screen.getByAltText('daterangepickervalue');
        expect(daterangepickervalue).toHaveAttribute("src",MultiDate1TillRange);
    });

    it('Renders the datepicker', async () => {
        render(<Datepicker />);

        const multidate = screen.getByPlaceholderText("Select dates");
        fireEvent.click(multidate);

        const daterange = screen.getByPlaceholderText("Select start & end date");
        fireEvent.click(daterange);
        
    });
    it("Renders the datepicker code sample ", () => {
        render(<Datepicker />);
        const { getByTestId } = render(<code />);
        const code = screen.getByTestId('librarycode');
        expect(code).toBeInTheDocument();
    });
    
});