import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import MultiDate1 from "../Assets/Multi2.png";
import MultiDatebydate from "../Assets/Multi3.png";
import MultiDateRange from "../Assets/Range2.png";
import MultiDate1TillRange from "../Assets/Range3.png";
import { CardContent } from "@mui/material";
import Card from "@mui/material/Card";
import Cards from "../Cards/Cards";
import Data from "../../Data/DatepickerData";
import ViewInArSharpIcon from "@mui/icons-material/ViewInArSharp";
import Grid4x4Icon from "@mui/icons-material/Grid4x4";
import AttractionsIcon from "@mui/icons-material/Attractions";
import MemoryIcon from "@mui/icons-material/Memory";
import IntegrationInstructionsIcon from "@mui/icons-material/IntegrationInstructions";
import { DatepickerCode } from "multiple-date-range-picker";
import CodePen from "../Icons/codepen.png";
import Command from "../Icons/command.png";
import Award from "../Icons/award.png";
import CPU from "../Icons/cpu.png";
import Autocomplete from "autocomplete-textfield";

const Datepicker = () => {
  return (
    <div>
      <Typography
        variant="h5"
        component="h5"
        role="heading"
        aria-level={5}
        className="topHeading"
      >
        Datepicker
      </Typography>
      <Typography component="p" className="topHeadingSub">
        Simple React datepicker component calendars with the ability to select
        the date in single, multiple and range modes.
      </Typography>
      <Typography
        variant="h6"
        component="div"
        style={{
          marginLeft: 10,
          marginTop: 54,
          display: "flex",
          position: "relative",
        }}
        role="heading"
        aria-level={6}
        className="titleHeading"
      >
        <img
          src={CodePen}
          alt="codePen"
          width="40px"
          height="40px"
          style={{ marginRight: "14px" }}
        />{" "}
        Installation
      </Typography>
      <p
        style={{
          marginTop: 18,
          marginBottom: 37,
          background: "#F4F4F4",
          height: 72,
          padding: 20,
          borderRadius: 11,
          boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
          fontFamily: "Helvetica Neue LT Pro",
          fontStyle: "normal",
          fontWeight: 500,
          fontSize: 18,
          lineHeight: 2,
          color: "#636161",
          paddingLeft: 38,
        }}
      >
        npm i multiple-date-range-picker
      </p>
      
      <Typography
        variant="h6"
        component="div"
        style={{ marginLeft: 10, display: "flex", position: "relative" }}
        role="heading"
        aria-level={6}
        className="titleHeading"
      >
        <img
          src={Command}
          alt="Command"
          width="40px"
          height="40px"
          style={{ marginRight: "14px" }}
        />
        Layout
      </Typography>
      <Typography
        component="p"
        className="topHeadingSub"
        style={{ marginBottom: 50, marginTop: 18, marginLeft: 55 }}
      >
        You’ll need to install React and PropTypes separately since those
        dependencies aren’t included in the package. If you need to use a locale
        other than the default en-US, you'll also need to import that into your
        project from date-fns (see Localization section below). Below is a
        simple example of how to use the Datepicker in a React view. You will
        also need to require the CSS file from this package (or provide your
        own). The example below shows how to include the CSS from this package
        if your build system supports requiring CSS files (Webpack is one that
        does).
      </Typography>
      <Box
      style={{
        marginBottom: 75,
        marginLeft: 55,
        backgroundColor: "#F3F3F3",
        paddingBottom: 12,
        paddingTop: 12,
        paddingLeft: 12,
      }}
    >
      <Grid container>
        <Grid item xs={3}>
          <img 
          src={MultiDate1} 
          alt="datepicker" 
          height="200" 
          width="200" 
          />
        </Grid>
        <Grid item xs={3}>
          <img
            src={MultiDatebydate}
            alt="multidatepickervalue"
            height="200"
            width="200"
          />
        </Grid>
        <Grid item xs={3}>
          <img
            src={MultiDateRange}
            alt="daterangepicker"
            height="200"
            width="200"
          />
        </Grid>
        <Grid item xs={3}>
          <img
            src={MultiDate1TillRange}
            alt="daterangepickervalue"
            height="200"
            width="200"
          />
        </Grid>
      </Grid>
      </Box>
      {/* <div style={{ flexDirection: "row" }}>
        <Typography variant="h6" component="div" style={{ marginBottom: 20 }}>
          <AttractionsIcon /> Default
        </Typography>{" "}
        <Typography component="div" style={{ marginBottom: 20 }}>
          The user can select multiple dates.
        </Typography>
        <Card
          style={{
            marginBottom: 20,
            background: "#ffff",
            width: "50%",
            borderRadius: 10,
          }}
        >
          <CardContent style={{ marginTop: 40 }}>
            <DatepickerCode />
          </CardContent>
        </Card>
        <Typography variant="h6" component="h6" style={{ marginBottom: 20 }}>
          <MemoryIcon /> Usage
        </Typography>
        <Card
          style={{
            marginBottom: 20,
            width: "50%",
            borderRadius: 10,
            background: "black",
          }}
        >
          <CardContent>
            <pre style={{ background: "black", color: "antiquewhite" }}>
              <code>{Data.DatepickerCode}</code>
            </pre>
          </CardContent>
        </Card>
      </div> */}

      <Box>
        <Grid container spacing={4}>
          <Grid item xs={6}>
            <Typography
              variant="h6"
              component="div"
              style={{ marginLeft: 10, display: "flex", position: "relative" }}
              role="heading"
              aria-level={6}
              className="titleHeading"
            >
              <img
                src={Award}
                alt="Award"
                width="40px"
                height="40px"
                style={{ marginRight: "14px" }}
              />
              Default View
            </Typography>
            <Typography
              component="p"
              style={{ marginBottom: 55, marginLeft: 67 }}
              className="topHeadingSub"
            >
              The user can select multiple dates and date range.
            </Typography>
            <Card 
              style={{
                background: "#F4F4F4",
                height: 301,
                width: 445,
                borderRadius: 11,
                boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
              }}
              >
              <CardContent>
              <div
                style={{
                  fontFamily: "Helvetica Neue LT Pro",
                  fontStyle: "normal",
                  fontWeight: 500,
                  fontSize: 16,
                  color: "#636161",
                  marginTop: 70,            
                }}
                className = "datePickerText"
              >
                <DatepickerCode multiple />
                <DatepickerCode />
                </div>
              </CardContent>
            </Card>
          </Grid>
          <Grid item xs={6}>
            <Typography
              variant="h6"
              component="div"
              style={{ marginLeft: 10, display: "flex", position: "relative" }}
              role="heading"
              aria-level={6}
              className="titleHeading"
            >
              <img
                src={CPU}
                alt="Award"
                width="40px"
                height="40px"
                style={{ marginRight: "14px" }}
              />
              Usage (Code View)
            </Typography>
            <Typography
              component="p"
              style={{ marginBottom: 55, marginLeft: 67 }}
              className="topHeadingSub"
            >
              Usage
            </Typography>
            <p
              style={{
                background: "#2D2D2D",
                height: 301,
                width: 445,
                borderRadius: 11,
                boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
                overflowY: 'scroll'
              }}
            >
              <div
                style={{
                  fontFamily: "Helvetica Neue LT Pro",
                  fontStyle: "normal",
                  fontWeight: 500,
                  fontSize: 16,
                  color: "#FFFFFF",
                  padding: "26px 24px 16px 50px",
                  whiteSpace: 'break-spaces'
                }}
              >
                {Data.DatepickerCode}
              </div>
            </p>
          </Grid>
        </Grid>
      </Box>
    </div>
  );
};
export default Datepicker;
