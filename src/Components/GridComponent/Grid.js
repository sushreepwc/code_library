import React from "react";
import Typography from "@mui/material/Typography";
import CodePen from "../Icons/codepen.png";
import Award from "../Icons/award.png";
import { Grid, Box } from "pwc-react-library";

const gridImport = `import { Grid } from "pwc-react-library";`;
const basicGrid = `
<Grid container spacing="sm">
  <Grid item lg={8}>
    <Box style={{backgroundColor: "white",color: "black",borderRadius: "4px"}}>lg=8</Box>
  </Grid>

  <Grid item lg={4}>
    <Box style={{backgroundColor: "white",color: "black",borderRadius: "4px"}}>lg=4</Box>
  </Grid>

  <Grid item lg={4}>
    <Box style={{backgroundColor: "white",color: "black",borderRadius: "4px"}}>xs=4</Box>
  </Grid>

  <Grid item lg={8}>
    <Box style={{backgroundColor: "white",color: "black",borderRadius: "4px"}}>xs=8</Box>
  </Grid>
</Grid>`;

const multipleBreakpointsGrids = `
<Grid container spacing="sm">
  <Grid item xs={6} lg={8}>
    <Box style={{backgroundColor: "#47b0b0",color: "#ede9f9",borderRadius: "4px"}}>xs=6 lg=8</Box>
  </Grid>

  <Grid item xs={6} lg={4}>
    <Box style={{backgroundColor: "#47b0b0",color: "#ede9f9",borderRadius: "4px"}}>xs=6 lg=4</Box>
  </Grid>

  <Grid item xs={6} lg={4}>
    <Box style={{backgroundColor: "#47b0b0",color: "#ede9f9",borderRadius: "4px"}}>xs=6 lg=4</Box>
  </Grid>

  <Grid item xs={6} lg={8}>
    <Box style={{backgroundColor: "#47b0b0",color: "#ede9f9",borderRadius: "4px"}}>xs=6 lg=8</Box>
  </Grid>
</Grid>`;

const spacingGrids = `
<Grid container spacing="sm">
  <Grid item lg={4}>
    <Box style={{backgroundColor: "#47b0b0",color: "#ede9f9",borderRadius: "4px"}}>small</Box>
  </Grid>

  <Grid item lg={4}>
    <Box style={{backgroundColor: "#47b0b0",color: "#ede9f9",borderRadius: "4px"}}>small</Box>
  </Grid>
</Grid>

<Grid container spacing="md">
  <Grid item lg={4}>
    <Box style={{backgroundColor: "#47b0b0",color: "#ede9f9",borderRadius: "4px"}}>medium</Box>
  </Grid>

  <Grid item lg={4}>
    <Box style={{backgroundColor: "#47b0b0",color: "#ede9f9",borderRadius: "4px"}}>medium</Box>
  </Grid>
</Grid>

<Grid container spacing="lg">
  <Grid item lg={4}>
    <Box style={{backgroundColor: "#47b0b0",color: "#ede9f9",borderRadius: "4px"}}>large</Box>
  </Grid>

  <Grid item lg={4}>
    <Box style={{backgroundColor: "#47b0b0",color: "#ede9f9",borderRadius: "4px"}}>large</Box>
  </Grid>
</Grid>`;

const justifyContentGrids = `
<Grid container spacing="sm" justifyContent="flex-start">
  <Grid item lg={8}>
    <Box style={{backgroundColor: "#47b0b0",color: "#ede9f9",borderRadius: "4px"}}>flex-start</Box>
  </Grid>
</Grid>

<Grid container spacing="sm" justifyContent="center">
  <Grid item lg={8}>
    <Box style={{backgroundColor: "#47b0b0",color: "#ede9f9",borderRadius: "4px"}}>center</Box>
  </Grid>
</Grid>

<Grid container spacing="sm" justifyContent="flex-end">
  <Grid item lg={6}>
    <Box style={{backgroundColor: "#47b0b0",color: "#ede9f9",borderRadius: "4px"}}>flex-end</Box>
  </Grid>
</Grid>

<Grid container spacing="sm" justifyContent="space-between">
  <Grid item lg={6}>
    <Box style={{backgroundColor: "#47b0b0",color: "#ede9f9",borderRadius: "4px"}}>space-between</Box>
  </Grid>
  <Grid item lg={6}>
    <Box style={{backgroundColor: "#47b0b0",color: "#ede9f9",borderRadius: "4px"}}>space-between</Box>
  </Grid>
</Grid>`;

const GridComponent = () => {
  const [showBasicGrids, setShowBasicGrids] = React.useState(false);
  const [showMultipleGrids, setShowMultipleGrids] = React.useState(false);
  const [showSpacingGrids, setShowSpacingGrids] = React.useState(false);
  const [showJustifyContentGrids, setShowJustifyContentGrids] =
    React.useState(false);

  return (
    <div>
      <Typography
        variant="h5"
        component="h5"
        role="heading"
        aria-level={5}
        className="topHeading"
      >
        Grid
      </Typography>
      <Typography component="p" className="topHeadingSub">
        The react library design responsive layout grid adapts to screen size
        and orientation, ensuring consistency across layouts.
      </Typography>
      <Typography
        variant="h6"
        component="div"
        style={{
          marginTop: 30,
          display: "flex",
          position: "relative",
        }}
        role="heading"
        aria-level={6}
        className="titleHeading"
        gutterBottom
      >
        <img
          src={CodePen}
          alt="codePen"
          width="40px"
          height="40px"
          style={{ marginRight: "14px" }}
        />{" "}
        Import
      </Typography>
      <p
        style={{
          background: "#2D2D2D",
          height: "auto",
          width: "auto",
          borderRadius: 11,
          boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
        }}
      >
        <div
          style={{
            fontFamily: "Helvetica Neue LT Pro",
            fontStyle: "normal",
            fontWeight: 500,
            fontSize: 16,
            color: "#E1C16E",
            padding: "20px",
            whiteSpace: "break-spaces",
          }}
        >
          {gridImport}
        </div>
      </p>
      <Typography
        variant="h6"
        component="div"
        style={{ display: "flex", position: "relative" }}
        role="heading"
        aria-level={6}
        className="titleHeading"
      >
        <img
          src={Award}
          alt="Award"
          width="40px"
          height="40px"
          style={{ marginRight: "14px" }}
        />
        Basic grid
      </Typography>
      <Typography
        component="p"
        style={{ marginBottom: 30, marginLeft: 57 }}
        className="topHeadingSub"
      >
        Column widths are integer values between 1 and 12; they apply at any
        breakpoint and indicate how many columns are occupied by the component.
        A value given to a breakpoint applies to all the other breakpoints wider
        than it. For example, xs={12} sizes a component to occupy the whole
        viewport width regardless of its size.
      </Typography>
      <p
        style={{
          height: "auto",
          width: "auto",
          borderRadius: 6.5,
          border: "2px solid grey",
          alignItems: "center",
          display: "flex",
          justifyContent: "center",
          position: "relative",
        }}
      >
        <div
          style={{
            fontFamily: "Helvetica Neue LT Pro",
            fontStyle: "normal",
            fontWeight: 500,
            fontSize: 16,
            padding: "20px",
          }}
        >
          <Grid container spacing="sm">
            <Grid item lg={8}>
              <Box
                style={{
                  backgroundColor: "#47b0b0",
                  color: "#ede9f9",
                  borderRadius: "4px",
                }}
              >
                lg=8
              </Box>
            </Grid>

            <Grid item lg={4}>
              <Box
                style={{
                  backgroundColor: "#47b0b0",
                  color: "#ede9f9",
                  borderRadius: "4px",
                }}
              >
                lg=4
              </Box>
            </Grid>

            <Grid item lg={4}>
              <Box
                style={{
                  backgroundColor: "#47b0b0",
                  color: "#ede9f9",
                  borderRadius: "4px",
                }}
              >
                xs=4
              </Box>
            </Grid>

            <Grid item lg={8}>
              <Box
                style={{
                  backgroundColor: "#47b0b0",
                  color: "#ede9f9",
                  borderRadius: "4px",
                }}
              >
                xs=8
              </Box>
            </Grid>
          </Grid>
          <br />
          <Grid item>
            <div className="show">
              <button
                onClick={() => setShowBasicGrids(!showBasicGrids)}
                className="btn"
              >
                {showBasicGrids ? "hide code" : "show code"}
              </button>
            </div>
          </Grid>
        </div>
      </p>
      {showBasicGrids && (
        <div>
          <p
            style={{
              background: "#2D2D2D",
              height: "auto",
              width: "auto",
              borderRadius: 11,
              boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
            }}
          >
            <div
              style={{
                fontFamily: "Helvetica Neue LT Pro",
                fontStyle: "normal",
                fontWeight: 500,
                fontSize: 16,
                color: "#E1C16E",
                padding: "20px",
                whiteSpace: "break-spaces",
              }}
            >
              {basicGrid}
            </div>
          </p>
        </div>
      )}
      <Typography
        variant="h6"
        component="div"
        style={{ display: "flex", position: "relative" }}
        role="heading"
        aria-level={6}
        className="titleHeading"
        gutterBottom
      >
        Grid with multiple breakpoints
      </Typography>
      <Typography
        component="p"
        style={{ marginBottom: 30 }}
        className="topHeadingSub"
        gutterBottom
      >
        Components may have multiple widths defined, causing the layout to
        change at the defined breakpoint. Width values given to larger
        breakpoints override those given to smaller breakpoints. For example,
        xs={6} lg={8} sizes a component to occupy half of the viewport width (6
        columns) when viewport width is 600 or more pixels. For smaller
        viewports, the component fills all 12 available columns.
      </Typography>
      <p
        style={{
          height: "auto",
          width: "auto",
          borderRadius: 6.5,
          border: "2px solid grey",
          alignItems: "center",
          display: "flex",
          justifyContent: "center",
          position: "relative",
        }}
      >
        <div
          style={{
            fontFamily: "Helvetica Neue LT Pro",
            fontStyle: "normal",
            fontWeight: 500,
            fontSize: 16,
            padding: "20px",
          }}
        >
          <Grid container spacing="sm">
            <Grid item xs={6} lg={8}>
              <Box
                style={{
                  backgroundColor: "#47b0b0",
                  color: "#ede9f9",
                  borderRadius: "4px",
                }}
              >
                xs=6 lg=8
              </Box>
            </Grid>

            <Grid item xs={6} lg={4}>
              <Box
                style={{
                  backgroundColor: "#47b0b0",
                  color: "#ede9f9",
                  borderRadius: "4px",
                }}
              >
                xs=6 lg=4
              </Box>
            </Grid>

            <Grid item xs={6} lg={4}>
              <Box
                style={{
                  backgroundColor: "#47b0b0",
                  color: "#ede9f9",
                  borderRadius: "4px",
                }}
              >
                xs=6 lg=4
              </Box>
            </Grid>

            <Grid item xs={6} lg={8}>
              <Box
                style={{
                  backgroundColor: "#47b0b0",
                  color: "#ede9f9",
                  borderRadius: "4px",
                }}
              >
                xs=6 lg=8
              </Box>
            </Grid>
          </Grid>
          <br />
          <Grid item>
            <div className="show">
              <button
                onClick={() => setShowMultipleGrids(!showMultipleGrids)}
                className="btn"
              >
                {showMultipleGrids ? "hide code" : "show code"}
              </button>
            </div>
          </Grid>
        </div>
      </p>
      {showMultipleGrids && (
        <div>
          <p
            style={{
              background: "#2D2D2D",
              height: "auto",
              width: "auto",
              borderRadius: 11,
              boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
            }}
          >
            <div
              style={{
                fontFamily: "Helvetica Neue LT Pro",
                fontStyle: "normal",
                fontWeight: 500,
                fontSize: 16,
                color: "#E1C16E",
                padding: "20px",
                whiteSpace: "break-spaces",
              }}
            >
              {multipleBreakpointsGrids}
            </div>
          </p>
        </div>
      )}
      <Typography
        variant="h6"
        component="div"
        style={{ display: "flex", position: "relative" }}
        role="heading"
        aria-level={6}
        className="titleHeading"
        gutterBottom
      >
        Spacing
      </Typography>
      <Typography
        component="p"
        style={{ marginBottom: 30 }}
        className="topHeadingSub"
        gutterBottom
      >
        To control space between children, use the spacing prop. The spacing
        value can be any string.
      </Typography>
      <p
        style={{
          height: "auto",
          width: "auto",
          borderRadius: 6.5,
          border: "2px solid grey",
          alignItems: "center",
          display: "flex",
          justifyContent: "center",
          position: "relative",
        }}
      >
        <div
          style={{
            fontFamily: "Helvetica Neue LT Pro",
            fontStyle: "normal",
            fontWeight: 500,
            fontSize: 16,
            padding: "20px",
          }}
        >
          <Grid container spacing="sm">
            <Grid item lg={4}>
              <Box
                style={{
                  backgroundColor: "#47b0b0",
                  color: "#ede9f9",
                  borderRadius: "4px",
                }}
              >
                small
              </Box>
            </Grid>

            <Grid item lg={4}>
              <Box
                style={{
                  backgroundColor: "#47b0b0",
                  color: "#ede9f9",
                  borderRadius: "4px",
                }}
              >
                small
              </Box>
            </Grid>
          </Grid>
          <Grid container spacing="md">
            <Grid item lg={4}>
              <Box
                style={{
                  backgroundColor: "#47b0b0",
                  color: "#ede9f9",
                  borderRadius: "4px",
                }}
              >
                medium
              </Box>
            </Grid>

            <Grid item lg={4}>
              <Box
                style={{
                  backgroundColor: "#47b0b0",
                  color: "#ede9f9",
                  borderRadius: "4px",
                }}
              >
                medium
              </Box>
            </Grid>
          </Grid>
          <Grid container spacing="lg">
            <Grid item lg={4}>
              <Box
                style={{
                  backgroundColor: "#47b0b0",
                  color: "#ede9f9",
                  borderRadius: "4px",
                }}
              >
                large
              </Box>
            </Grid>

            <Grid item lg={4}>
              <Box
                style={{
                  backgroundColor: "#47b0b0",
                  color: "#ede9f9",
                  borderRadius: "4px",
                }}
              >
                large
              </Box>
            </Grid>
          </Grid>
          <br />
          <Grid item>
            <div className="show">
              <button
                onClick={() => setShowSpacingGrids(!showSpacingGrids)}
                className="btn"
              >
                {showSpacingGrids ? "hide code" : "show code"}
              </button>
            </div>
          </Grid>
        </div>
      </p>
      {showSpacingGrids && (
        <div>
          <p
            style={{
              background: "#2D2D2D",
              height: "auto",
              width: "auto",
              borderRadius: 11,
              boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
            }}
          >
            <div
              style={{
                fontFamily: "Helvetica Neue LT Pro",
                fontStyle: "normal",
                fontWeight: 500,
                fontSize: 16,
                color: "#E1C16E",
                padding: "20px",
                whiteSpace: "break-spaces",
              }}
            >
              {spacingGrids}
            </div>
          </p>
        </div>
      )}
      <Typography
        variant="h6"
        component="div"
        style={{ display: "flex", position: "relative" }}
        role="heading"
        aria-level={6}
        className="titleHeading"
        gutterBottom
      >
        JustifyContent
      </Typography>
      <Typography
        component="p"
        style={{ marginBottom: 30 }}
        className="topHeadingSub"
        gutterBottom
      >
        To maintain space between grids and align grid content to flex-start,
        center, flex-end, space-between attribute values. And even alignItems
        also works same.
      </Typography>
      <p
        style={{
          height: "auto",
          width: "auto",
          borderRadius: 6.5,
          border: "2px solid grey",
          alignItems: "center",
          display: "flex",
          justifyContent: "center",
          position: "relative",
        }}
      >
        <div
          style={{
            fontFamily: "Helvetica Neue LT Pro",
            fontStyle: "normal",
            fontWeight: 500,
            fontSize: 16,
            padding: "20px",
          }}
        >
          <Grid container spacing="sm" justifyContent="flex-start">
            <Grid item lg={8}>
              <Box
                style={{
                  backgroundColor: "#47b0b0",
                  color: "#ede9f9",
                  borderRadius: "4px",
                }}
              >
                flex-start
              </Box>
            </Grid>
          </Grid>

          <Grid container spacing="sm" justifyContent="center">
            <Grid item lg={8}>
              <Box
                style={{
                  backgroundColor: "#47b0b0",
                  color: "#ede9f9",
                  borderRadius: "4px",
                }}
              >
                center
              </Box>
            </Grid>
          </Grid>

          <Grid container spacing="sm" justifyContent="flex-end">
            <Grid item lg={6}>
              <Box
                style={{
                  backgroundColor: "#47b0b0",
                  color: "#ede9f9",
                  borderRadius: "4px",
                }}
              >
                flex-end
              </Box>
            </Grid>
          </Grid>

          <Grid container spacing="sm" justifyContent="space-between">
            <Grid item lg={6}>
              <Box
                style={{
                  backgroundColor: "#47b0b0",
                  color: "#ede9f9",
                  borderRadius: "4px",
                }}
              >
                space-between
              </Box>
            </Grid>
            <Grid item lg={6}>
              <Box
                style={{
                  backgroundColor: "#47b0b0",
                  color: "#ede9f9",
                  borderRadius: "4px",
                }}
              >
                space-between
              </Box>
            </Grid>
          </Grid>
          <br />
          <Grid item>
            <div className="show">
              <button
                onClick={() =>
                  setShowJustifyContentGrids(!showJustifyContentGrids)
                }
                className="btn"
              >
                {showJustifyContentGrids ? "hide code" : "show code"}
              </button>
            </div>
          </Grid>
        </div>
      </p>
      {showJustifyContentGrids && (
        <div>
          <p
            style={{
              background: "#2D2D2D",
              height: "auto",
              width: "auto",
              borderRadius: 11,
              boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
            }}
          >
            <div
              style={{
                fontFamily: "Helvetica Neue LT Pro",
                fontStyle: "normal",
                fontWeight: 500,
                fontSize: 16,
                color: "#E1C16E",
                padding: "20px",
                whiteSpace: "break-spaces",
              }}
            >
              {justifyContentGrids}
            </div>
          </p>
        </div>
      )}
    </div>
  );
};

export default GridComponent;
