import React from "react";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";
import CodePen from "../Icons/codepen.png";
import Award from "../Icons/award.png";
import { TextField } from "pwc-react-library";

const textFieldImport = `import { TextField } from "pwc-react-library";`;
const basicTextFieldData = [
  {
    variant: "outline",
    label: "Outline",
    placeholder: "Outline",
  },
  {
    variant: "standard",
    label: "Standard",
    placeholder: "Standard",
  },
  {
    variant: "filled",
    label: "Filled",
    placeholder: "Filled",
  },
];

const basicTextFieldCode = [`<TextField label="Outline" placeholder="Outline" variant="outline" />`, `<TextField label="Standard" placeholder="Standard" variant="standard" />`, `<TextField label="Filled" placeholder="Filled" variant="filled" />`];

const formPropsTextFieldCode = [
  `<TextField label="Required *" placeholder="Required *" variant="outline" defaultValue={"Hello World"} required />`,
  `<TextField label="Disabled" placeholder="Disabled" variant="outline" defaultValue={"Hello World"} disabled />`,
  `<TextField type="password" label="Password" placeholder="Password" variant="outline" />`,
  `<TextField label="Helper Text" placeholder="Helper Text" helpText="Some important text" variant="outline" defaultValue={"Default Value"} />`,

  `<TextField label="Required *" placeholder="Required *" variant="filled" defaultValue={"Hello World"} required />`,
  `<TextField label="Disabled" placeholder="Disabled" variant="filled" defaultValue={"Hello World"} disabled />`,
  `<TextField type="password" label="Password" placeholder="Password" variant="filled" />`,
  `<TextField label="Helper Text" placeholder="Helper Text" helpText="Some important text" variant="filled" defaultValue={"Default Value"} />`,

  `<TextField label="Required *" placeholder="Required *" variant="standard" defaultValue={"Hello World"} required />`,
  `<TextField label="Disabled" placeholder="Disabled" variant="standard" defaultValue={"Hello World"} disabled />`,
  `<TextField type="password" label="Password" placeholder="Password" variant="standard" />`,
  `<TextField label="Helper Text" placeholder="Helper Text" helpText="Some important text" variant="standard" defaultValue={"Default Value"} />`,
];

const errorTextFieldCode = [
  `<TextField label="Error" placeholder="Error" variant="outline" defaultValue={"Hello World"} error={true} />`,
  `<TextField label="Error" placeholder="Error" variant="outline" defaultValue={"Hello World"} error={true} helpText="Incorrect entry." />`,

  `<TextField label="Error" placeholder="Error" variant="filled" defaultValue={"Hello World"} error={true} />`,
  `<TextField label="Error" placeholder="Error" variant="filled" defaultValue={"Hello World"} error={true} helpText="Incorrect entry." />`,

  `<TextField label="Error" placeholder="Error" variant="stndard" defaultValue={"Hello World"} error={true} />`,
  `<TextField label="Error" placeholder="Error" variant="stndard" defaultValue={"Hello World"} error={true} helpText="Incorrect entry." />`,
];

const Typhography = () => {
  const [showBasicTextField, setShowBasicTextField] = React.useState(false);
  const [showDefaultValueTextField, setShowDefaultValueTextField] = React.useState(false);
  const [showErrorTextField, setShowErrorTextField] = React.useState(false);

  return (
    <div>
      <Typography variant="h5" component="h5" role="heading" aria-level={5} className="topHeading">
        Text Field
      </Typography>
      <Typography component="p" className="topHeadingSub">
        Text Fields allow users enter and edit text.
      </Typography>
      <Typography
        variant="h6"
        component="div"
        style={{
          marginTop: 30,
          display: "flex",
          position: "relative",
        }}
        role="heading"
        aria-level={6}
        className="titleHeading"
        gutterBottom
      >
        <img src={CodePen} alt="codePen" width="40px" height="40px" style={{ marginRight: "14px" }} /> Import
      </Typography>
      <p
        style={{
          background: "#2D2D2D",
          height: "auto",
          width: "auto",
          borderRadius: 11,
          boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
        }}
      >
        <div
          style={{
            fontFamily: "Helvetica Neue LT Pro",
            fontStyle: "normal",
            fontWeight: 500,
            fontSize: 16,
            color: "#E1C16E",
            padding: "20px",
            whiteSpace: "break-spaces",
          }}
        >
          {textFieldImport}
        </div>
      </p>
      <Typography variant="h6" component="div" style={{ display: "flex", position: "relative" }} role="heading" aria-level={6} className="titleHeading">
        <img src={Award} alt="Award" width="40px" height="40px" style={{ marginRight: "14px" }} />
        Basic Text Field
      </Typography>
      <Typography component="p" style={{ marginBottom: 30, marginLeft: 57 }} className="topHeadingSub">
        Text Field comes with different variants like outlined, standard and filled. It support help text, shwoing error and error state for text field.
      </Typography>
      <p
        style={{
          height: "auto",
          width: "auto",
          borderRadius: 6.5,
          border: "2px solid grey",
          alignItems: "center",
          display: "flex",
          justifyContent: "center",
          position: "relative",
        }}
      >
        <div
          style={{
            fontFamily: "Helvetica Neue LT Pro",
            fontStyle: "normal",
            fontWeight: 500,
            fontSize: 16,
            padding: "20px",
            width: "100%",
          }}
        >
          <Grid container spacing={2}>
            {basicTextFieldData.map((data) => (
              <Grid item xs={12} md={6}>
                <TextField label={data?.label} placeholder={data?.placeholder} variant={data?.variant} />
              </Grid>
            ))}
            <Grid item>
              <div className="show">
                <button onClick={() => setShowBasicTextField(!showBasicTextField)} className="btn">
                  {showBasicTextField ? "hide code" : "show code"}
                </button>
              </div>
            </Grid>
          </Grid>
        </div>
      </p>
      {showBasicTextField && (
        <div>
          <p
            style={{
              background: "#2D2D2D",
              height: "auto",
              width: "auto",
              borderRadius: 11,
              boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
            }}
          >
            <div
              style={{
                fontFamily: "Helvetica Neue LT Pro",
                fontStyle: "normal",
                fontWeight: 500,
                fontSize: 16,
                color: "#E1C16E",
                padding: "20px",
                whiteSpace: "break-spaces",
              }}
            >
              {basicTextFieldCode.map((code) => (
                <>
                  {code}
                  <br />
                </>
              ))}
            </div>
          </p>
        </div>
      )}
      <br />
      <Typography variant="h6" component="div" style={{ display: "flex", position: "relative" }} role="heading" aria-level={6} className="titleHeading">
        Form props
      </Typography>
      <Typography component="p">Standard form attibutes are supported e.g required, disabled, type etc. Support helpText for show help text or error messages.</Typography>
      <br />
      <p
        style={{
          height: "auto",
          width: "auto",
          borderRadius: 6.5,
          border: "2px solid grey",
          alignItems: "center",
          display: "flex",
          justifyContent: "center",
          position: "relative",
        }}
      >
        <div
          style={{
            fontFamily: "Helvetica Neue LT Pro",
            fontStyle: "normal",
            fontWeight: 500,
            fontSize: 16,
            padding: "20px",
            width: "100%",
          }}
        >
          <Grid container spacing={2}>
            <Grid item xs={12} md={6}>
              <TextField label="Required *" placeholder="Required *" variant="outline" defaultValue={"Hello World"} required />
            </Grid>
            <Grid item xs={12} md={6}>
              <TextField label="Disabled" placeholder="Disabled" variant="outline" defaultValue={"Hello World"} disabled />
            </Grid>
            <Grid item xs={12} md={6}>
              <TextField type="password" label="Password" placeholder="Password" variant="outline" />
            </Grid>
            <Grid item xs={12} md={6}>
              <TextField label="Helper Text" placeholder="Helper Text" helpText="Some important text" variant="outline" defaultValue={"Default Value"} />
            </Grid>

            <Grid item xs={12} md={6}>
              <TextField label="Required *" placeholder="Required *" variant="filled" defaultValue={"Hello World"} required />
            </Grid>
            <Grid item xs={12} md={6}>
              <TextField label="Disabled" placeholder="Disabled" variant="filled" defaultValue={"Hello World"} disabled />
            </Grid>
            <Grid item xs={12} md={6}>
              <TextField type="password" label="Password" placeholder="Password" variant="filled" />
            </Grid>
            <Grid item xs={12} md={6}>
              <TextField label="Helper Text" placeholder="Helper Text" helpText="Some important text" variant="filled" defaultValue={"Default Value"} />
            </Grid>

            <Grid item xs={12} md={6}>
              <TextField label="Required *" placeholder="Required *" variant="standard" defaultValue={"Hello World"} required />
            </Grid>
            <Grid item xs={12} md={6}>
              <TextField label="Disabled" placeholder="Disabled" variant="standard" defaultValue={"Hello World"} disabled />
            </Grid>
            <Grid item xs={12} md={6}>
              <TextField type="password" label="Password" placeholder="Password" variant="standard" />
            </Grid>
            <Grid item xs={12} md={6}>
              <TextField label="Helper Text" placeholder="Helper Text" helpText="Some important text" variant="standard" defaultValue={"Default Value"} />
            </Grid>

            <Grid item>
              <div className="show">
                <button onClick={() => setShowDefaultValueTextField(!showDefaultValueTextField)} className="btn">
                  {showDefaultValueTextField ? "hide code" : "show code"}
                </button>
              </div>
            </Grid>
          </Grid>
        </div>
      </p>

      {showDefaultValueTextField && (
        <div>
          <p
            style={{
              background: "#2D2D2D",
              height: "auto",
              width: "auto",
              borderRadius: 11,
              boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
            }}
          >
            <div
              style={{
                fontFamily: "Helvetica Neue LT Pro",
                fontStyle: "normal",
                fontWeight: 500,
                fontSize: 16,
                color: "#E1C16E",
                padding: "20px",
                whiteSpace: "break-spaces",
              }}
            >
              {formPropsTextFieldCode.map((code) => (
                <>
                  {code}
                  <br />
                </>
              ))}
            </div>
          </p>
        </div>
      )}

      <Typography variant="h6" component="div" style={{ display: "flex", position: "relative" }} role="heading" aria-level={6} className="titleHeading">
        Validation
      </Typography>
      <Typography component="p">The error Prop used for toggle the error and helpText prop can be used for show the error message.</Typography>

      <br />
      <p
        style={{
          height: "auto",
          width: "auto",
          borderRadius: 6.5,
          border: "2px solid grey",
          alignItems: "center",
          display: "flex",
          justifyContent: "center",
          position: "relative",
        }}
      >
        <div
          style={{
            fontFamily: "Helvetica Neue LT Pro",
            fontStyle: "normal",
            fontWeight: 500,
            fontSize: 16,
            padding: "20px",
            width: "100%",
          }}
        >
          <Grid container spacing={2}>
            <Grid item xs={12} md={6}>
              <TextField label="Error" placeholder="Error" variant="outline" defaultValue={"Hello World"} error={true} />
            </Grid>
            <Grid item xs={12} md={6}>
              <TextField label="Error" placeholder="Error" variant="outline" defaultValue={"Hello World"} error={true} helpText="Incorrect entry." />
            </Grid>

            <Grid item xs={12} md={6}>
              <TextField label="Error" placeholder="Error" variant="filled" defaultValue={"Hello World"} error={true} />
            </Grid>
            <Grid item xs={12} md={6}>
              <TextField label="Error" placeholder="Error" variant="filled" defaultValue={"Hello World"} error={true} helpText="Incorrect entry." />
            </Grid>

            <Grid item xs={12} md={6}>
              <TextField label="Error" placeholder="Error" variant="standard" defaultValue={"Hello World"} error={true} />
            </Grid>
            <Grid item xs={12} md={6}>
              <TextField label="Error" placeholder="Error" variant="standard" defaultValue={"Hello World"} error={true} helpText="Incorrect entry." />
            </Grid>

            <Grid item>
              <div className="show">
                <button onClick={() => setShowErrorTextField(!showErrorTextField)} className="btn">
                  {showErrorTextField ? "hide code" : "show code"}
                </button>
              </div>
            </Grid>
          </Grid>
        </div>
      </p>

      {showErrorTextField && (
        <div>
          <p
            style={{
              background: "#2D2D2D",
              height: "auto",
              width: "auto",
              borderRadius: 11,
              boxShadow: "inset -4px 5px 33px -12px rgba(0, 0, 0, 0.08)",
            }}
          >
            <div
              style={{
                fontFamily: "Helvetica Neue LT Pro",
                fontStyle: "normal",
                fontWeight: 500,
                fontSize: 16,
                color: "#E1C16E",
                padding: "20px",
                whiteSpace: "break-spaces",
              }}
            >
              {errorTextFieldCode.map((code) => (
                <>
                  {code}
                  <br />
                </>
              ))}
            </div>
          </p>
        </div>
      )}
      <br />
    </div>
  );
};
export default Typhography;
