const AccordionCode = `import { AccordionComponent } from "react_accordion_pwc";

const Accordion = () => {
  return (
    <AccordionComponent
      title="Custom accordion open by default"
      defaultOpen={true}
    >
      <div>
        Lorem Ipsum is simply dummy text of the printing and typesetting
        industry. Lorem Ipsum has been the industry's standard dummy text ever
        since the 1500s, when an unknown printer took a galley of type and
        scrambled it to make a type specimen book
      </div>
    </AccordionComponent>
  );
};

export default Accordion;

`
export default {AccordionCode};