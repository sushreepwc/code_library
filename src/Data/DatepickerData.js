const DatepickerCode = `import { DatepickerCode } from "multiple-date-range-picker";

const Datepicker = () => {
  return (
    <DatepickerCode multiple/>
    <DatepickerCode/>
  );
};

export default Datepicker;`;

export default { DatepickerCode };
