const DropDownOptions = [
  {
    label: "Light",
    value: "light",
    children: new Array(20).fill(null).map((_, index) => {
      return { label: `Number ${index}`, value: index };
    }),
  },
  {
    label: "Bamboo",
    value: "bamboo",
    children: [
      { label: "Toy Fish", value: "fish" },
      { label: "Toy Cards", value: "cards" },
      { label: "Toy Bird", value: "bird" },
    ],
  },
];

const DropDownCode = `import React from "react";
import { CascadedSelectBox } from "code-library-dropdown/dist/components";
 
function App(){
  const options = [
    {
      label:"Light",
      label:"light",
      children: [
        {label : Number 0, value: 0 },
        {label : Number 1, value: 1 },
        {label : Number 2, value: 2 },
        {label : Number 3, value: 3 },
        {label : Number 4, value: 4 },
        {label : Number 5, value: 5 },
        {label : Number 6, value: 6 },
        {label : Number 7, value: 7 },
        {label : Number 8, value: 8 },
        {label : Number 9, value: 9 },
        {label : Number 10, value: 10 },
        {label : Number 11, value: 11 },
        {label : Number 12, value: 12 },
        {label : Number 13, value: 13 },
        {label : Number 14, value: 14 },
        {label : Number 15, value: 15 },
        {label : Number 16, value: 16 },
        {label : Number 17, value: 17 },
        {label : Number 18, value: 18 },
        {label : Number 19, value: 19 },    
      ]
    },
    {
      label: "Bamboo",
      value: 'bamboo',
      children: [
        { label: "Toy Fish", value: "fish" },
        { label: "Toy Cards", value: "cards" },
        { label: "Toy Bird", value: "bird" }
      ]
    }
  ];

  return (
    <div>
      <CascadedSelectBox onChange={onChange} options={options} />
    </div>
  );
};

export default App;

`

export default { DropDownOptions, DropDownCode };
